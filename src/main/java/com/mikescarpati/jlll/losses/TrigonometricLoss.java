package com.mikescarpati.jlll.losses;

import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;

public class TrigonometricLoss extends LossType{
	/**
	 * Use the following to ensure that the loss is finite. If 
	 * y *<w,x> < MAX_ERR_TO_COMPUTE, return a cached value. 
	 */
	private static float MIN_SCORE_TO_COMPUTE = -0.9f;
	private static float MAX_LOSS = (float) (2*Math.log(1/Math.cos((Math.PI/4.0)*(1-MIN_SCORE_TO_COMPUTE))));
	@Override
	public float predict(WeightVector w, SparseVector x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getLoss(WeightVector w, SparseVector x) {
		float yTimesScore = x.getY() * w.innerProduct(x);
		if(yTimesScore >= 1){
			return 0;			
		} if(yTimesScore <= MIN_SCORE_TO_COMPUTE){
			return MAX_LOSS;
		}
		return (float) (2*Math.log(1/Math.cos((Math.PI/4.0)*(1-yTimesScore))));
		
	}

	@Override
	public float getUpdateDirection(WeightVector w, SparseVector x) {
		// return (float) (x.getY() / (1 + Math.exp(x.getY() * w.innerProduct(x))));
		// update is w += x * rate * updateDirection
		// this loss has gradient -y * pi/2 * tan((pi/4)*(1-y*<w,x>))
		// TODO: Fix this
		return 0;
	}

}
