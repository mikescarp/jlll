/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import static org.fest.assertions.Assertions.assertThat;
import java.util.ArrayList;

import org.testng.annotations.Test;

public class JenkinsHasherTest {
	
	@Test
	public void testMask(){
		JenkinsHasher jh = new JenkinsHasher();
		assertThat(jh.getHashMask(4)).isEqualTo(15);

		  
	}
	
	@Test
	public void testSingleInt(){
		JenkinsHasher jh = new JenkinsHasher();
		int mask22 = jh.getHashMask(22);
		assertThat(jh.hash(66, mask22)).isEqualTo(3436118);
	}
	
	@Test
	public void testTwoInt(){
		JenkinsHasher jh = new JenkinsHasher();
		int mask22 = jh.getHashMask(22);
		assertThat(jh.hash(87,71, mask22)).isEqualTo(4111611);
	}
	
	@Test
	public void testArrayListHash(){
		JenkinsHasher jh = new JenkinsHasher();
		int mask22 = jh.getHashMask(22);
		ArrayList<Integer> vInts = new ArrayList<Integer>();
		for (int i = 1; i < 10; ++i) {
		    vInts.add(i);
		}
		//assertThat(jh.hash(vInts, mask22)).isEqualTo(3190187);
		//Java overflows at a different point because it doesn't
		// have an unsigned int type.  I'm going to keep this until I
		// find out that it is a problem.
		assertThat(jh.hash(vInts, mask22)).isEqualTo(2380307);
	}
	
}
