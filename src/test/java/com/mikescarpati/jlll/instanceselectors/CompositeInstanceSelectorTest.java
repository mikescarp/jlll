/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.instanceselectors;

import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;

import org.testng.annotations.Test;

public class CompositeInstanceSelectorTest {
	@Test
	public void testInitialization() throws IOException{
		DataSet ds = new DataSet("src/test/resources/sf-data-set_test.dat", 5, true);
		InstanceSelector selector = new CompositeInstanceSelector(
				new BalancedStochasticSelector(),
				new StochasticSelector());
		assertThat(selector.isInitialized()).isFalse();
		selector.initializeFrom(ds);
		assertThat(selector.isInitialized()).isTrue();
	}
	
	@Test
	public void testGetNextRanks() throws IOException{
		DataSet ds = new DataSet("src/test/resources/sf-data-set_test.dat", 5, true);
		InstanceSelector selector = new CompositeInstanceSelector(
				new StochasticROCSelector(),
				new StochasticRankSelector(), 0.6f);
		selector.initializeFrom(ds);
		boolean allSame = true;
		SparseVector x, lastX;
		x = selector.getNextInstance();
		lastX = x;
		for(int i=0; i<50; i++){
			assertThat(ds.contains(x)).isFalse(); // ranking, so this should be a difference
			if(!x.equals(lastX)){
				allSame = false;
			}
			lastX = x;
			x = selector.getNextInstance();
		}
		assertThat(allSame).isFalse();
	}
	
	@Test
	public void testGetNextNonRanks() throws IOException{
		DataSet ds = new DataSet("src/test/resources/sf-data-set_test.dat", 5, true);
		InstanceSelector selector = new CompositeInstanceSelector(
				new BalancedStochasticSelector(),
				new StochasticSelector(), 0.3f);
		selector.initializeFrom(ds);
		boolean allSame = true;
		SparseVector x, lastX;
		x = selector.getNextInstance();
		lastX = x;
		for(int i=0; i<50; i++){
			assertThat(ds.contains(x)).isTrue();
			if(!x.equals(lastX)){
				allSame = false;
			}
			lastX = x;
			x = selector.getNextInstance();
		}
		assertThat(allSame).isFalse();
	}
	
	@Test
	public void testGetNextCRR() throws IOException{
		DataSet ds = new DataSet("src/test/resources/sf-data-set_test.dat", 5, true);
		InstanceSelector selector = new CompositeInstanceSelector(
				new StochasticROCSelector(),
				new StochasticSelector());
		selector.initializeFrom(ds);
		boolean allSame = true;
		boolean allSameType = true; //shouldn't be all rank or straight
		SparseVector x, lastX;
		x = selector.getNextInstance();
		lastX = x;
		for(int i=0; i<50; i++){
			if(!(ds.contains(x) == ds.contains(lastX))){
				allSameType = false;
			}
			if(!x.equals(lastX)){
				allSame = false;
			}
			lastX = x;
			x = selector.getNextInstance();
		}
		assertThat(allSame).isFalse();
		assertThat(allSameType).isFalse();
	}
}
