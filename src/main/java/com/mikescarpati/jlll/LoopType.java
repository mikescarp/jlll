/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

public enum LoopType {
	STOCHASTIC, //normal stochastic sampling for stochastic gradient descent
	BALANCED_STOCHASTIC, //balanced sampling from positives and negatives
	RANK, //indexed sampling of candidate pairs for pairwise learning to rank
	ROC, //indexed sampling to optimize ROC Area
	QUERY_NORM_RANK, //sample candidate pairs, give equal weight to each qid group
	COMBINED_RANKING, //CRR algorithm for combined regression and ranking
	COMBINED_ROC, //CRR algorithm for combined regression and ROC area optimization
	SS_RANK //Semi-supervised ranking with indexed sampling of pairs
}
