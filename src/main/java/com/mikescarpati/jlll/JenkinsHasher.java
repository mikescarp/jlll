/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import java.util.ArrayList;

/**
 *  This is a port of Bob Jenkins's one-at-a-time-hash, modified
 *  to hash int's rather than char's.  In particular, we're interested
 *  having fast methods for exactly one key, and for two or more keys.
 *  Because of this small number of keys, we perform the first two steps
 *  of the hash twice on the first key considered.
 *  
 *  Note that a key of 0 returns a hash of 0.
 *  Hash Function for a single int.*/
public class JenkinsHasher {
	
	/**
	 * Hashes a single integer
	 * @param key
	 * @param mask
	 * @return
	 */
	int hash(Integer key, int mask) {
	  int hash = key;
	  hash += (hash << 10);
	  hash ^= (hash >> 6);
	  hash += (hash << 3);
	  hash ^= (hash >> 11);
	  hash += (hash << 15);
	  return hash & mask;
	}

	/**
	 * Hash function for two int's.
	 * @param key_1
	 * @param key_2
	 * @param mask
	 * @return
	 */
	int hash(Integer key1, Integer key2, int mask) {
	  int hash = key1;
	  hash += (hash << 10);
	  hash ^= (hash >> 6);

	  hash += key2;
	  hash += (hash << 10);
	  hash ^= (hash >> 6);

	  hash += (hash << 3);
	  hash ^= (hash >> 11);
	  hash += (hash << 15);
	  return hash & mask;
	}

	/**
	 * Hash function for a vector of int's.  Assumes that keys has
	 * at least one entry.
	 * @param keys
	 * @param mask
	 * @return
	 */
	int hash(ArrayList<Integer> keys, int mask) {
	  int hash = 0;
	  for (int i = 0; i < keys.size(); ++i) {
	    hash += keys.get(i);
	    hash += (hash << 10);
	    hash ^= (hash >> 6);
	  }
	  hash += (hash << 3);
	  hash ^= (hash >> 11);
	  hash += (hash << 15);
	  return hash & mask;
	}

	/**
	 * Helper function to get the appropriate mask for the desired number 
	 * of bits.
	 * @param numBits
	 * @return
	 */
	int getHashMask(int numBits) {
	  int mask = 1;
	  for (int i = 1; i < numBits; ++i) {
	    mask += (1 << i);
	  }
	  return mask;
	}
}
