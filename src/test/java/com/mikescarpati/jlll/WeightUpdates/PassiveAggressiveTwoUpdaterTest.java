/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.WeightUpdates;

import static org.fest.assertions.Assertions.assertThat;

import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.OptimizationContext;
import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.losses.HingeLoss;
import com.mikescarpati.jlll.losses.LossType;
import com.mikescarpati.jlll.rates.ConstantRate;

import org.testng.annotations.Test;

public class PassiveAggressiveTwoUpdaterTest {

	
	@Test
	public void testSingleUpdate(){
		float lambda = 0.1f;
		float capacity = 1.0f;
		WeightVector w0 = new WeightVector(2);
		SparseVector x = new SparseVector("1.0 0:1 1:1");
		// set capacity to 1, so put 1/2=0.5 on denominator
		WeightUpdater wu = new PassiveAggressiveTwoUpdater(capacity);
		OptimizationContext oc = new OptimizationContext(lambda, new ConstantRate());
		LossType loss = new HingeLoss();
		//takeStep(WeightVector w, SparseVector x, OptimizationContext oc, LossType p)
		WeightVector w1 = wu.takeStep(w0, x, oc, loss);
		// prediction would be 0 -> loss=1 -> step=1/(2+0.5)=0.4		
		assertThat(w1.getValue(0)).isEqualTo(0.4f);
		assertThat(w1.getValue(1)).isEqualTo(0.4f);
	}
	
	@Test
	public void testNonUpdate(){
		float lambda = 0.1f;
		float capacity = 1.0f;
		WeightVector w0 = new WeightVector(2);
		SparseVector x = new SparseVector("0.0 0:1 1:1");
		// set capacity to 1, so put 1/2=0.5 on denominator
		WeightUpdater wu = new PassiveAggressiveTwoUpdater(capacity);
		OptimizationContext oc = new OptimizationContext(lambda, new ConstantRate());
		LossType loss = new HingeLoss();
		//takeStep(WeightVector w, SparseVector x, OptimizationContext oc, LossType p)
		WeightVector w1 = wu.takeStep(w0, x, oc, loss);
		// no step should be taken with hinge loss when y=0
		assertThat(w1.getValue(0)).isEqualTo(0.0f);
		assertThat(w1.getValue(1)).isEqualTo(0.0f);
	}
}
