/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import com.mikescarpati.jlll.rates.LearningRate;

/**
 * Keeps track of a few stateful items needed in optimization.
 * @author Mike Scarpati
 *
 */
public class OptimizationContext {
	LearningRate rate;
	float lambda;
	float rateMultiplier;

	/**
	 * New context with regularization/capacity lambda and learning rate 
	 * to be returned by rate.
	 * @param lambda float, regularization amount
	 * @param rate LearningRate defining the learning rate schedule
	 */
	public OptimizationContext(float lambda, LearningRate rate){
		this.rate = rate;
		this.rate.reset();
		this.lambda = lambda;
		this.rateMultiplier = 1.0f;
	}
	
	public float getLambda(){
		return lambda;
	}
	public void setLambda(float lambda){
		this.lambda = lambda;
	}
	/**
	 * The learning rates have a fixed scale by default, this allows them to be
	 * scaled without implementing a new LearningRate.  The rate used will be
	 * rate.getRate * rateMultiplier
	 * @param x amount to multiply the current rate by 
	 */
	public void setRateMultiplier(float x){
		this.rateMultiplier = x;
	}
	public float getRate(){
		return rate.getRate() * rateMultiplier;
	}
	/**
	 * To be called after a step is taken in order to update the learning rate.
	 */
	public void nextStep(){
		rate.increment();
	}
	public String getRateClass(){
		return rate.getClass().getName();
	}
	public void reset(){
		rate.reset();
	}
	public int getIteration(){
		return rate.getIteration();
	}
}
