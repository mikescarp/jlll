## JLLL ##

JLLL stands for Java Linear Learning Library, and is essentially
a port of D Sculley's
[sofia-ml](https://code.google.com/p/sofia-ml/). The primary
difference between this project and that are
- This is designed for use as a library rather than a command-line
  tool
- I've broken the pieces into a design that's easier for me to extend
- Not every option is available, but there are a couple of new
  features

### Usage ###

    DataSet train = new DataSet("myTrainData.svmlight", 8, false);
    DataSet testData = new DataSet("myTestData.svmlight", 8, false);

    Learner learn = new Learner.Builder(trainData)
                               .selector(new StochasticROCSelector())
                               .rate(new BasicRate())
                               .build();
    learn.train();

    ArrayList<Float> predictions = learn.predict(testData);

### Performance ###

The more flexible architecture and switch to Java came with a slight
performance penalty.  I haven't done very thorough tests, but it seems
that the pairwise methods take roughly twice as long, and the
single-instance methods take a similar amount of time.  I haven't
benchmarked the data-reading code, but that was quite fast for me.

### The Architecture ###

I may end up changing the names of some classes, but right now, there
are a couple of key parts of each Learner.
1. **InstanceSelector** will determine how instances get presented to
   the rest of the learning algorithm. It's designed so that the other
   pieces don't care how it was selected--it might be a difference of
   two vectors (as with ROC or ranking), or it could be a single
   vector. It could also be something that sometimes gives a raw
   vector and sometimes a difference (as it the Combined Regression
   and Ranking paper).
2. **WeightUpdater** determines how to calculate the updates at each
   time step. One is a simple SGD implementation, another is PEGASOS
   (which does regularization, and projection in each step), and we
   also have two variants of the Passive-Aggressive algorithm. The others
   are per-coordinate ones that often work better but take a bit more
   time and memory.
3. **LossFunction** lets you do regression, hinge loss, or logistic
   regression using the same framework. I've also implement a few less
   common loss functions that I used to experiment with when dealing
   with noisy data.
4. **LearningRate** might be better referred to as a learning rate
   schedule--do we always update by the same amount, or anneal in a
   certain way?

### The Tests ###
There should be pretty good test coverage, with two different types of
tests.  The individual components have unit tests that ensure that
they behave properly, and the Learner class has a suite that generates
some easy-to-learn data, and tries learning with every feasible
combination of parts.  If it can't learn very well on this simple
data, the test will fail.

### Status ###
The key pieces of the codebase work, but there are some parts that
haven't been thoroughly tested or cleaned up. In particular, Use of the
HashWeightVector should be used with caution, and the fit statistics
classes (BinaryFit and RegressionFit) are highly lacking in terms of
code-cleanliness, testing, and documentation.

This code is unsupported--it was created for my thesis research and
I am unable to devote time to it anymore.

### License ###
Apache 2.0, see LICENSE for terms and conditions

### Author ###
Written by Mike Scarpati
