package com.mikescarpati.jlll.util;

import com.mikescarpati.jlll.BinaryFit;

public class EvaluationResult {
	
	BinaryFit bf;
	double trainROC;
	String review;
	String description;	
	
	public EvaluationResult(BinaryFit bf, String review, String description, double trainROC){
		this.bf = bf;
		this.review = review;
		this.description = description;
		this.trainROC = trainROC;
	}
	
	@Override
	public String toString(){
		return String.format("Review: %s\tClassifier: %s\t%g\t%g\t%g\t%g", review, description, bf.getAUROC(), bf.getSens(), bf.getSpec(), trainROC);
	}

}
