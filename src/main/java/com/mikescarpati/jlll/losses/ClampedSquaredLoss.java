/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.losses;

import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;

/**
 * Lets us handle regression problems.
 * @author Mike Scarpati
 *
 */
public class ClampedSquaredLoss extends LossType {
	float min;
	float max;
	float epsilon; // don't move if within this of true
	boolean clampUpdates;
	
	public ClampedSquaredLoss(float min, float max, float epsilon, boolean clampUpdates){
		this.min = min;
		this.max = max;
		this.epsilon = epsilon;
		this.clampUpdates = clampUpdates;
	}
	@Override
	public float predict(WeightVector w, SparseVector x) {
		return clamp(w.innerProduct(x));
	}

	private float clamp(float x){
		if(x<min)
			return min;
		if(x>max)
			return max;
		return x;					
	}
	@Override
	public float getLoss(WeightVector w, SparseVector x) {
		if(clampUpdates){
			float pred = clamp(w.innerProduct(x));
			float err = Math.abs(x.getY() - pred) - epsilon;
			return err>0 ? (err * err) : 0;			
		} else{
			float err = x.getY() - w.innerProduct(x);
			return (err * err);
		}
		
	}

	@Override
	public float getUpdateDirection(WeightVector w, SparseVector x) {
		if(clampUpdates){
			float pred = clamp(w.innerProduct(x));
			return Math.abs(x.getY() - pred)>epsilon ? x.getY() - pred : 0;			
		}
		else{
			return x.getY() - w.innerProduct(x);
		}
	}

}
