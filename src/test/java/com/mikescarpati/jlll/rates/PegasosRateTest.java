/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.rates;

import org.testng.annotations.Test;
import static org.fest.assertions.Assertions.assertThat;

public class PegasosRateTest {
	
	// Note: formula = 1/(lambda*i)
	@Test(dataProviderClass=RateTestCases.class,dataProvider = "lambdas")
	public void testInitial(float lambda){
		LearningRate lr = new PegasosRate(lambda);
		assertThat(lr.getRate()).isEqualTo(1.0f/(lambda * 1), RateTestCases.delta);
	}
	
	@Test(dataProviderClass=RateTestCases.class,dataProvider = "ratesWithLambda")
	public void testIncrement(int numIncrements, float lambda){
		LearningRate lr = new PegasosRate(lambda);
		int i=1;
		while(i < numIncrements){
			lr.increment();
			i++;
		}
		assertThat(lr.getRate()).isEqualTo(1.0f/(lambda * i), RateTestCases.delta);
	}
}
