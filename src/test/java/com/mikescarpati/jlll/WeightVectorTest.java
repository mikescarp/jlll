/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

public class WeightVectorTest {

	@Test
	public void testFiveDimensional(){
		WeightVector w = new WeightVector(5);
		assertThat(w.getDimensions()).isEqualTo(5);
		assertThat(w.getSquaredNorm()).isEqualTo(0.0f);
		assertThat(w.getValue(4)).isEqualTo(0.0f);
	}
	
	@Test
	public void testBasicOperations() throws Exception{
		WeightVector w5 = new WeightVector(5);
		SparseVector s = new SparseVector("1.0 0:1 1:1.0 2:2.0 4:3.0");
		assertThat(w5.innerProduct(s)).isEqualTo(0.0f);
		w5.addVector(s, 2.0f);
		assertThat(w5.getValue(4)).isEqualTo(6.0f);
		assertThat(w5.getValue(6)).isEqualTo(0.0f);
		assertThat(w5.getSquaredNorm()).isEqualTo(60.0f);
		assertThat(w5.innerProduct(s)).isEqualTo(30.0f);
		
		w5.scaleBy(0.5);
		assertThat(w5.getSquaredNorm()).isEqualTo(15.0f);
		assertThat(w5.getValue(4)).isEqualTo(3.0f);
		w5.toString();
		assertThat(w5.getSquaredNorm()).isEqualTo(15.0f);
		assertThat(w5.getValue(4)).isEqualTo(3.0f);
	}
	
	@Test
	public void testFromString(){
		WeightVector w = new WeightVector("3.0 2.0 -1.0");
		assertThat(w.getDimensions()).isEqualTo(3);
		assertThat(w.getValue(0)).isEqualTo(3.0f);
		assertThat(w.getValue(1)).isEqualTo(2.0f);
		assertThat(w.getValue(2)).isEqualTo(-1.0f);
		assertThat(w.getSquaredNorm()).isEqualTo(14.0f);
		assertThat(w.toString()).isEqualTo("3.0 2.0 -1.0");
	}

	@Test
	public void testDifferences(){
		WeightVector w = new WeightVector("0 1 2 3 4");
		SparseVector a = new SparseVector("1.0 1:1 2:1.5 4:-2.5");
		SparseVector b = new SparseVector("1.0 1:-1 2:1.5 3:2");
		SparseVector abDiff = new SparseVector(a, b, 1.0f);
		assertThat(w.innerProduct(abDiff, 1.0f)).isEqualTo(
				w.innerProductOnDifference(a,  b, 1.0f));
	}
	
	@Test
	public void testProjection1(){
		WeightVector w = new WeightVector("0 1 2 3 4");
		w.projectToL1Ball(3.0f);
		assertThat(w.getValue(0)).isEqualTo(0.0f);
		assertThat(w.getValue(1)).isEqualTo(0.0f);
		assertThat(w.getValue(2)).isEqualTo(0.0f);
		assertThat(w.getValue(3)).isEqualTo(1.0f);
		assertThat(w.getValue(4)).isEqualTo(2.0f);
		assertThat(w.getSquaredNorm()).isEqualTo(5.0f);
	}
	
	@Test
	public void testCopy(){
		WeightVector w = new WeightVector("0 1 2 3 -4 -5");
		WeightVector wc = new WeightVector(w);
		assertThat(wc.getValue(0)).isEqualTo(0.0f);
		assertThat(wc.getValue(1)).isEqualTo(1.0f);
		assertThat(wc.getValue(2)).isEqualTo(2.0f);
		assertThat(wc.getValue(3)).isEqualTo(3.0f);
		assertThat(wc.getValue(4)).isEqualTo(-4.0f);
		assertThat(wc.getValue(5)).isEqualTo(-5.0f);
		assertThat(wc.getSquaredNorm()).isEqualTo(w.getSquaredNorm());
		assertThat(wc.getDimensions()).isEqualTo(w.getDimensions());
	}
	
	@Test
	public void testProjection2(){
		WeightVector w = new WeightVector("0 1 2 3 -4 -5");
		w.projectToL1Ball(20.0f);
		assertThat(w.getValue(0)).isEqualTo(0.0f);
		assertThat(w.getValue(1)).isEqualTo(1.0f);
		assertThat(w.getValue(2)).isEqualTo(2.0f);
		assertThat(w.getValue(3)).isEqualTo(3.0f);
		assertThat(w.getValue(4)).isEqualTo(-4.0f);
		assertThat(w.getValue(5)).isEqualTo(-5.0f);
		
		w.projectToL1Ball(6.0f);
		assertThat(w.getValue(0)).isEqualTo(0.0f);
		assertThat(w.getValue(1)).isEqualTo(0.0f);
		assertThat(w.getValue(2)).isEqualTo(0.0f);
		assertThat(w.getValue(3)).isEqualTo(1.0f);
		assertThat(w.getValue(4)).isEqualTo(-2.0f);
		assertThat(w.getValue(5)).isEqualTo(-3.0f);
		
		w.projectToL1Ball(0.0f);
		assertThat(w.getValue(0)).isEqualTo(0.0f);
		assertThat(w.getValue(1)).isEqualTo(0.0f);
		assertThat(w.getValue(2)).isEqualTo(0.0f);
		assertThat(w.getValue(3)).isEqualTo(0.0f);
		assertThat(w.getValue(4)).isEqualTo(0.0f);
		assertThat(w.getValue(5)).isEqualTo(0.0f);
	}
	  
}
