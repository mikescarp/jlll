/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import java.util.Random;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;

/**
 * The simplest implementation, this just randomly picks one instance from
 * the dataset and provides it on getNextInstance.
 * @author Mike Scarpati
 */
public class StochasticSelector implements InstanceSelector {

	Random r;
	boolean initialized;
	DataSet data;
	
	public StochasticSelector(){
		r = new Random();
		initialized = false;
	}
	
	@Override
	public void initializeFrom(DataSet data){
		this.data = data;
		initialized = true;
	}
	/** 
	 * Randomly picks one instance from the dataset and returns it.
	 */
	@Override
	public SparseVector getNextInstance() {
		return data.vectorAt(r.nextInt(data.numExamples()));
	}

	@Override
	public boolean isInitialized() {
		// TODO Auto-generated method stub
		return initialized;
	}

}
