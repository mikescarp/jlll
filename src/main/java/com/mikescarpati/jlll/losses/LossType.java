/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.losses;

import java.util.ArrayList;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;

public abstract class LossType {
	public abstract float predict(WeightVector w, SparseVector x);
	public abstract float getLoss(WeightVector w, SparseVector x);
	public abstract float getUpdateDirection(WeightVector w, SparseVector x);
	
	public ArrayList<Float> predictAll(DataSet data, WeightVector w){
		ArrayList<Float> preds = new ArrayList<Float>(data.numExamples());
		for(SparseVector x : data){
			preds.add(predict(w, x));
		}
		return preds;
	}
	public float objectiveValue(DataSet data, WeightVector w, float lambda){
		float obj =  0;
		for(SparseVector x : data){
			obj += getLoss(w, x);
		}
		obj /= data.numExamples();
		return obj + (float) (w.getSquaredNorm() * lambda / 2.0f);
	}
}
