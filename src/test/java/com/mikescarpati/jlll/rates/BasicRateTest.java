/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.rates;

import org.testng.annotations.Test;

import static org.fest.assertions.Assertions.assertThat;

public class BasicRateTest {

	
	// Note: formula = 10/(10+i)
	@Test
	public void testInit(){
		LearningRate lr = new BasicRate();
		assertThat(lr.getRate()).isEqualTo((10.0f/11.0f), RateTestCases.delta);
	}
	
	@Test(dataProviderClass=RateTestCases.class,dataProvider = "rates")
	public void testIncrement(int numIncrements){
		LearningRate lr = new BasicRate();
		int i = 1;
		while(i < numIncrements){
			lr.increment();
			i++;
		}
		assertThat(lr.getRate()).isEqualTo((10.0f/(10.0f+i)), RateTestCases.delta);
	}
}
