/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import java.util.ArrayList;

import com.mikescarpati.jlll.WeightUpdates.PegasosUpdater;
import com.mikescarpati.jlll.WeightUpdates.WeightUpdater;
import com.mikescarpati.jlll.instanceselectors.StochasticSelector;
import com.mikescarpati.jlll.rates.LearningRate;
import com.mikescarpati.jlll.rates.PegasosRate;
import com.mikescarpati.jlll.util.CrossValidator;
import org.testng.log4testng.Logger;

import com.mikescarpati.jlll.instanceselectors.InstanceSelector;
import com.mikescarpati.jlll.losses.HingeLoss;
import com.mikescarpati.jlll.losses.LossType;

/**
 * The class that most users will interact with.  Its main abilities are to
 * train on a given DataSet, then make predictions.  Because there are so many
 * optional parameters and Java doesn't support named arguments, it uses the Builder
 * pattern (see Joshua Bloch's Effective Java for more info).  Example construction:
 * 
 * 		Learner learn = new Learner.Builder(LearnerTestCases.genClassifierData(dim, numObs, 0.001f))
 *		                           .dimensions(dim+1)
 *		                           .selector(new StochasticROCSelector())
 *		                           .updater(new SgdUpdater())
 *		                           .rate(new BasicRate())
 *		                           .loss(new LogRegLoss())
 *		                           .build();
 * 
 * @author Mike Scarpati
 *
 */
public class Learner implements ILearner {
	DataSet data;
	InstanceSelector selector;
	protected OptimizationContext context;
	WeightUpdater updater;
	WeightVector weights;
	LossType loss;
	int dimension;
	int iterations;
	int projectL1every;
	float projectL1lambda;
	float projectL1epsilon;
	
	private static Logger log = Logger.getLogger(Learner.class);
	
	public static class Builder {
		// Required parameters
		private final DataSet data;
		
		// Optional Parameters with defaults
		private float lambda = 0.1f;
		private LearningRate rate = new PegasosRate(lambda);
		private InstanceSelector selector = new StochasticSelector();
		private int dimension = 131072;
		private int iterations = 100000;
		private float rateMultiplier = 1f;
		private WeightUpdater updater = new PegasosUpdater();
		private LossType loss = new HingeLoss();
		private OptimizationContext context; // initialized in build
		private int projectL1every = -1;
		private float projectL1lambda = 10.0f;
		private float projectL1epsilon = 1e-2f;
		
		public Builder(DataSet data){
			this.data = data;
		}
		public Builder lambda(float val)
			{lambda = val; 			return this;}
		public Builder rate(LearningRate r)
			{rate = r;				return this;}
		public Builder selector(InstanceSelector s)
			{selector = s;			return this;}
		public Builder dimensions(int val)
			{dimension = val;		return this;}
		public Builder iterations(int val)
			{iterations = val;		return this;}
		public Builder loss(LossType lt)
			{loss = lt;				return this;}
		public Builder updater(WeightUpdater up)
			{updater = up;			return this;}
		public Builder rateMultiplier(float val)
			{rateMultiplier = val;	return this;}
		public Builder projectL1every(int val)
			{projectL1every = val;  return this;}
		public Builder projectL1lambda(float val)
			{projectL1lambda = val;  return this;}
		public Builder projectL1epsilon(float val)
			{projectL1epsilon = val;  return this;}
		public Learner build(){
			selector.initializeFrom(data);
			context = new OptimizationContext(lambda, rate);
			context.setRateMultiplier(rateMultiplier);
			return new Learner(this);
		}
		
	}
	
	/**
	 * This constructor is the most common way to create a new Learner, 
	 * but it is only exposed to the Builder class.
	 * @param builder
	 */
	protected Learner(Builder builder){
		this.data = builder.data;
		this.selector = builder.selector;
		this.context = builder.context;
		this.dimension = builder.dimension;
		this.iterations = builder.iterations;
		this.loss = builder.loss;
		this.updater = builder.updater;
		this.weights = new WeightVector(dimension);
		this.projectL1every = builder.projectL1every;
		this.projectL1lambda = builder.projectL1lambda;
		this.projectL1epsilon = builder.projectL1epsilon;
	}
	
	Learner() {
		// All items are set manually, this is only used internally for cloning
	}

	/* (non-Javadoc)
	 * @see ILearner#train()
	 */
	public void train(){
		if(!selector.isInitialized()){
			selector.initializeFrom(data);
		}
		long startTime = System.currentTimeMillis();
		if(projectL1every<=0){
			for(int i=0; i<iterations; i++){
				weights = updater.takeStep(weights, selector.getNextInstance(), context, loss);
				context.nextStep();
			}			
		}else{
			for(int i=0; i<iterations; i++){
				weights = updater.takeStep(weights, selector.getNextInstance(), context, loss);
				if(i>10 && (i%projectL1every == 0)){
					weights.projectToL1Ball(projectL1lambda, projectL1epsilon);
				}
				context.nextStep();
			}
		}
		
		log.debug(String.format("Time taken: %.2f seconds", ((System.currentTimeMillis() - startTime)/1000.0f)));
	}
	
	/* (non-Javadoc)
	 * @see ILearner#predict(DataSet)
	 */
	public ArrayList<Float> predict(DataSet testData){
		return loss.predictAll(testData, weights);
	}
	/* (non-Javadoc)
	 * @see ILearner#predict()
	 */
	public ArrayList<Float> predict(){
		return loss.predictAll(data, weights);
	}
	
	/* (non-Javadoc)
	 * @see ILearner#predictOne(SparseVector)
	 */
	public float predictOne(SparseVector input){
		return loss.predict(weights, input);
	}
	
	/* (non-Javadoc)
	 * @see ILearner#objectiveValue(DataSet)
	 */
	public float objectiveValue(DataSet testData){
		return loss.objectiveValue(testData, weights, context.getLambda());
	}
	
	public float weightAt(int idx){
		return weights.getValue(idx);
	}
	
	public WeightVector getWeights(){
		return weights;
	}
	
	/**
	 * Using AUROC, get the best rate multiplier out of the provided array 
	 * @param rates
	 * @param folds
	 * @return
	 */
	public Pair<Float,Double> getOptimalRateMultiplier(float[] rates, int folds){
		double bestAUROC = 0;
		float bestRateMult = 0;
		for(float rate : rates){
			context.setRateMultiplier(rate);
			CrossValidator cv = new CrossValidator(folds, this);
			try {
				double curVal = cv.crossValidationResults().getAUROC();
				if(curVal > bestAUROC){
					bestAUROC = curVal;
					bestRateMult = rate;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		context.setRateMultiplier(bestRateMult);
		System.out.println(bestAUROC);
		return new Pair<Float,Double>(bestRateMult, bestAUROC);
	}
	
	/**
	 * Needs a new dataset to initialize selector from.
	 * @param x
	 * @return
	 */
	public Learner shallowClone(DataSet x){
		Learner ret = new Learner();
		ret.selector = selector;
		ret.selector.initializeFrom(x);
		ret.data = x;
		ret.context = new OptimizationContext(context.lambda, context.rate);
		ret.context.setRateMultiplier(context.rateMultiplier);
		ret.updater = updater.shallowClone();
		ret.weights = new WeightVector(weights.dimensions);
		ret.loss = loss;
		ret.dimension = dimension;
		ret.iterations = iterations;
		ret.projectL1every = projectL1every;
		ret.projectL1lambda = projectL1lambda;
		ret.projectL1epsilon = projectL1epsilon;
		
		return ret;
		
	}
	
	/* (non-Javadoc)
	 * @see ILearner#toString()
	 */
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("Learner with loss type ")
		  .append(loss.getClass().getName())
		  .append(" and selector type ")
		  .append(selector.getClass().getName())
		  .append(" and updating using ")
		  .append(updater.getClass().getName())
		  .append(" using a ")
		  .append(context.getRateClass())
		  .append(" on a data set with ")
		  .append(data.numExamples())
		  .append(" instances and ")
		  .append(dimension)
		  .append("dimensions");
		
		return sb.toString();
	}
	
	/* (non-Javadoc)
	 * @see ILearner#shortString()
	 */
	public String shortString(){
		StringBuilder sb = new StringBuilder();
		sb.append(loss.getClass().getName())
		  .append(", ")
		  .append(selector.getClass().getName())
		  .append(", ")
		  .append(context.rateMultiplier)
		  .append(", ")
		  .append(updater.getClass().getName());
		
		return sb.toString();
	}
	
	/* (non-Javadoc)
	 * @see ILearner#getDataSet()
	 */
	public DataSet getDataSet(){
		return data;
	}
	public void setDataSet(DataSet x){
		data = x;
	}
	public float getLambda(){
		return context.getLambda();
	}
	public LossType getLossFunc(){
		return loss;
	}
	
	public void reset(){
		weights = new WeightVector(weights.dimensions);
		context.reset();
	}

	public DataSet getPositives(Float thresh) {
		DataSet ds = data.clone();
		ds.clear();
		ArrayList<Float> preds = predict();
		for(int i=0; i<data.size(); i++){
			if(preds.get(i)>=thresh){
				ds.add(data.get(i));
			}
		}
		return ds;
	}

}
