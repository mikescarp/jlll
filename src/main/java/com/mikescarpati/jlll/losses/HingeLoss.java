/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.losses;

import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;

/**
 * Loss is max(0, 1-(y * <w,x>))
 * @author Mike Scarpati
 *
 */
public class HingeLoss extends LossType {

	@Override
	public float predict(WeightVector w, SparseVector x) {
		return w.innerProduct(x);
	}

	@Override
	public float getLoss(WeightVector w, SparseVector x) {
		return (x.getY() == 0) ? 0f : Math.max(0, 1 - (x.getY() * w.innerProduct(x)));
	}

	@Override
	public float getUpdateDirection(WeightVector w, SparseVector x) {
		return x.getY();
	}

}
