package com.mikescarpati.jlll.util;

import java.util.ArrayList;
import java.util.TreeSet;

import com.mikescarpati.jlll.BinaryFit;

public class ThresholdSelector {
	public static float findThreshForSensitivityGoal(ArrayList<Float> y, ArrayList<Float> preds, float sensGoal){
		TreeSet<Float> predictedProbs = new TreeSet<Float>();
		Float thresh = 1.0f;
		double sens = 0.0;
		for(Float pred: preds){
			predictedProbs.add(pred);
		}
		predictedProbs.add(thresh); //Should pick 0.5 if it meets the goal
		while(thresh != null){
			sens = sensitivityAtThresh(thresh, y, preds);
			if(sens>=sensGoal){
				return thresh;
			}
			thresh = predictedProbs.lower(thresh);
		}
		return 0.0f;
	}
	private static float sensitivityAtThresh(float thresh, ArrayList<Float> y, ArrayList<Float> preds){
		return (float) new BinaryFit(y, preds, thresh).getSens();
	}
}
