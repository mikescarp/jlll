/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;

/**
 * Picks instances to give to the learning algorithm. Flexibility here
 * lets us select between ranking and regression and classification seamlessly.
 * @author Mike Scarpati
 *
 */
public interface InstanceSelector {
	void initializeFrom(DataSet data);
	boolean isInitialized();
	SparseVector getNextInstance();
}
