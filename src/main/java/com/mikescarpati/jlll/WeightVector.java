/**
 * 
 */
package com.mikescarpati.jlll;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.*;

/**
 * This is basically just a float[] with some convenience methods.
 * Note that it has the capability to perform fast projection to 
 * an L1 ball, but that isn't used anywhere yet.
 * @author Mike Scarpati
 *
 */
public class WeightVector {
	// Obtain a logger
	private static Logger logger = Logger.getLogger("WeightVector");
	//The MIN_SCALING_FACTOR is used to protect against combinations of
	// lambda * eta > 1.0, which will cause numerical problems for regularization
	// and PEGASOS projection.  
	private static float MIN_SCALING_FACTOR = 0.0000001f;
	float[] weights;
	double scale;
	double squaredNorm;
	int dimensions;


	/**
	 *  Construct a weight vector of dimension d, with all weights initialized to
	 *  zero.
	 * @param dimensionality How big should the array be?
	 */	
	public WeightVector(int dimensionality){
		scale = 1.0f;
		dimensions = dimensionality;
		weights = new float[dimensions];
		if(dimensions<=0){
			logger.severe("Illegal dimensionality of weight vector less than 1."+
					"\nDimensions: " + dimensions + "\n");
			throw new IllegalArgumentException();
		}
		Arrays.fill(weights, 0);
	}
	
	/**
	 *  Construct a weight vector of dimension d, with all weights initialized to
	 *  fillValue.
	 * @param dimensionality How big should the array be?
	 * @param fillValue the value that will be in each element 
	 */	
	public WeightVector(int dimensionality, float fillValue){
		scale = 1.0f;
		dimensions = dimensionality;
		weights = new float[dimensions];
		if(dimensions<=0){
			logger.severe("Illegal dimensionality of weight vector less than 1."+
					"\nDimensions: " + dimensions + "\n");
			throw new IllegalArgumentException();
		}
		Arrays.fill(weights, fillValue);
	}

	/**
	 * Constructs a weight vector from a string, which is identical in format
	 * to that produced by the toString() member method.
	 * @param weightVectorString A string with space separators
	 */
	WeightVector(String weightVectorString){
		scale = 1.0f;
		String[] result = weightVectorString.split("\\s");
		// Count dimensions in string.
		dimensions = result.length;
		// Allocate weights
		try{
			weights = new float[dimensions];  
		}catch(OutOfMemoryError e){
			logger.severe("Not enough memory for weight vector of dimension: "+
					dimensions + "\n");
			throw e;
		}
		// Fill weights_ from weights in string.
		for (int i = 0; i < dimensions; ++i) {
			weights[i] = Float.parseFloat(result[i]);
			squaredNorm += weights[i] * weights[i];
		}
	}

	/**
	 *  Simple copy constructor, needed to allocate a new array of weights.
	 * @param weightVector vector to copy
	 */
	public WeightVector(WeightVector weightVector){
		scale = weightVector.scale;
		squaredNorm = weightVector.squaredNorm;
		dimensions = weightVector.dimensions;
		// Allocate weights
		try{
			weights = new float[dimensions];  
		}catch(OutOfMemoryError e){
			logger.severe("Not enough memory for weight vector of dimension: "+
					dimensions + "\n");
			throw e;
		}
		for (int i = 0; i < dimensions; ++i) {
			weights[i] = weightVector.weights[i];
		}
	}

	/**
	 * Assigns the weights of this vector to be the inverse of the one passed
	 * @param w
	 */
	public void setAsInverse(WeightVector w){
		assert(w.dimensions == dimensions);
		w.scaleToOne();
		squaredNorm = 0f;
		for(int i=0; i<dimensions; i++){
			float f = 1/w.weights[i];
			this.weights[i] = f;
			squaredNorm += f*f;
		}
		this.scale = 1f;
	}

	/**
	 * Re-scales weight vector to scale of 1, and then outputs each weight in
	 * order, space separated.
	 */
	@Override
	public String toString(){
		scaleToOne();
		String txt = "";
		for (int i = 0; i < dimensions; ++i) {
			txt += Float.toString(weights[i]);
			if (i < (dimensions - 1)) {
				txt += " ";
			}
		}
		return txt;
	}
	
	/**
	 * Computes inner product of <x_scale * x, w>
	 * @param x take product with this
	 * @param xScale how much should x be scaled before multiplying
	 * @return the value
	 */
	public float innerProduct(SparseVector x, float xScale){
		float innerProd = 0.0f;
		for (int i = 0; i < x.numFeatures(); ++i) {
			innerProd += weights[x.featureAt(i)] * x.valueAt(i);
		}
		innerProd *= xScale;
		innerProd *= scale;
		return innerProd;
	}
	/**
	 * Computes inner product of <x, w>
	 * @param x take product with this
	 * @return the value
	 */
	public float innerProduct(SparseVector x){
		return innerProduct(x, 1.0f);
	}

	/**
	 * Computes inner product of <x_scale * (a - b), w>
	 * @param a
	 * @param b
	 * @param xScale how much should x be scaled before multiplying
	 * @return the value
	 */
	public float innerProductOnDifference(SparseVector a, SparseVector b, float xScale){
		//   <x_scale * (a - b), w>
		// = <x_scale * a - x_scale * b, w>
		// = <x_scale * a, w> + <-1.0 * x_scale * b, w>
		float innerProd = 0.0f;
		innerProd += innerProduct(a, xScale);
		innerProd += innerProduct(b, -1.0f * xScale);
		return innerProd;
	}
	/**
	 * Computes inner product of <(a - b), w>
	 * @param a
	 * @param b
	 * @return the value
	 */
	public float innerProductOnDifference(SparseVector a, SparseVector b){
		return innerProductOnDifference(a, b, 1.0f);
	}

	/**
	 * w += x_scale * x
	 * @param x take product with this
	 * @param xScale how much should x be scaled before multiplying
	 */
	public void addVector(SparseVector x, float x_scale) throws Exception{
		if (x.featureAt(x.numFeatures() - 1) > dimensions) {
			logger.severe("Feature " + x.featureAt(x.numFeatures() - 1) +
					" exceeds dimensionality of weight vector: "
					+ dimensions +"\n" + x.toString());
			throw new Exception();
		}
		float innerProduct = 0.0f;
		for (int i = 0; i < x.numFeatures(); ++i) {
			float thisXValue = (float) x.valueAt(i) * x_scale;
			int thisXFeature = x.featureAt(i);
			innerProduct += weights[thisXFeature] * thisXValue;
			weights[thisXFeature] += thisXValue / scale;
		}
		squaredNorm += x.getSquaredNorm() * x_scale * x_scale +
				(2.0 * scale * innerProduct); 
	}

	/**
	 * w_i += x_scale * x_i * x_i
	 * @param x take product with this
	 * @param xScale how much should x be scaled before multiplying
	 */
	public void addSquaredVector(SparseVector x, float x_scale) throws Exception{
		if (x.featureAt(x.numFeatures() - 1) > dimensions) {
			logger.severe("Feature " + x.featureAt(x.numFeatures() - 1) +
					" exceeds dimensionality of weight vector: "
					+ dimensions +"\n" + x.toString());
			throw new Exception();
		}
		float innerProduct = 0.0f;
		for (int i = 0; i < x.numFeatures(); ++i) {
			float thisXValue = (float) x.valueAt(i) * x_scale;
			thisXValue *= thisXValue;
			int thisXFeature = x.featureAt(i);
			innerProduct += weights[thisXFeature] * thisXValue;
			weights[thisXFeature] += thisXValue / scale;
		}
		squaredNorm += x.getSquaredNorm() * x_scale * x_scale +
				(2.0 * scale * innerProduct); 
	}
	
	/**
	 * w_i += x_scale * (1/sqrt(eta_i)) * x_i
	 * @param x take product with this
	 * @param xScale how much should x be scaled before multiplying
	 */
	public void addElementwiseInvRootProduct(WeightVector eta, SparseVector x, float x_scale) throws Exception{
		if (x.featureAt(x.numFeatures() - 1) > dimensions) {
			logger.severe("Feature " + x.featureAt(x.numFeatures() - 1) +
					" exceeds dimensionality of weight vector: "
					+ dimensions +"\n" + x.toString());
			throw new Exception();
		}
		float innerProduct = 0.0f;
		for (int i = 0; i < x.numFeatures(); ++i) {
			int thisXFeature = x.featureAt(i);
			float thisXValue = (float) ((float) x.valueAt(i) * x_scale * (1f/ (1+Math.sqrt(eta.getValue(thisXFeature)))));
			innerProduct += weights[thisXFeature] * thisXValue;
			weights[thisXFeature] += thisXValue / scale;
		}
		squaredNorm += x.getSquaredNorm() * x_scale * x_scale +
				(2.0 * scale * innerProduct); 
	}
	/**
	 * w *= x_scale
	 * @param scalingFactor 
	 */
	public void scaleBy(double scalingFactor) throws Exception{
		// Take care of any numerical difficulties.
		if (scale < 0.00000000001) scaleToOne();

		// Do the scaling.
		squaredNorm *= (scalingFactor * scalingFactor);
		if (scalingFactor > 0.0) {
			scale *= scalingFactor;
		} else {
			logger.severe("Error: scaling weight vector by non-positive value!\n " +
					"This can cause numerical errors in PEGASOS projection.\n " +
					"This is likely due to too large a value of eta * lambda.\n ");
			throw new Exception();
		}
	}

	/**
	 * Returns value of element w[index], taking internal scaling into account.
	 * @param index
	 * @return value of element w[index]
	 */
	public float getValue(int index){
		if(index<0){
			logger.severe("Illegal index in getValue: " + index);
			throw new IllegalArgumentException();
		} else if(index>=dimensions){
			return 0.0f;
		}
		return (float) (weights[index] * scale);
	}
	
	/**
	 * Sets the value at the specified location to val.
	 * @param index
	 * @param val
	 */
	public void setValue(int index, float val){
		if(scale != 1){
			scaleToOne();
		}
		weights[index] = val;
	}
	/**
	 * Project this vector into the L1 ball of radius lambda.
	 * @param lambda radius of ball in L-1 space
	 */ 
	public void projectToL1Ball(float lambda){
		// Bail out early if possible.
		float currentL1 = 0.0f;
		for (int i = 0; i < dimensions; ++i) {
			if (Math.abs(getValue(i)) > 0) currentL1 += Math.abs(getValue(i));
		}
		if (currentL1 < lambda) return;

		ArrayList<Integer> U = new ArrayList<Integer>();
		ArrayList<Integer> L = new ArrayList<Integer>();
		ArrayList<Integer> G = new ArrayList<Integer>();

		ArrayList<Integer> temp;
		// Populate U with indices of all non-zero elements in weight vector.
		for(int i = 0; i<dimensions; i++){
			if (Math.abs(getValue(i)) > 0) {
				U.add(i);
				currentL1 += Math.abs(getValue(i));
			}
		}
		// Find the value of theta.
		double partial_pivot = 0;
		double partial_sum = 0;
		Random random = new Random();
		while (U.size() > 0) {
			G.clear();
			L.clear();
			// k is a random selection from U
			int k = U.get(random.nextInt(U.size()));
			float pivot_k = Math.abs(getValue(k));
			float partial_sum_delta = Math.abs(getValue(k));
			float partial_pivot_delta = 1.0f;
			// Partition U using pivot_k.
			for (int i = 0; i < U.size(); ++i) {
				float w_i = Math.abs(getValue(U.get(i))); //value in this in index U[i]
				if (w_i >= pivot_k) {
					if (U.get(i) != k) {
						partial_sum_delta += w_i;
						partial_pivot_delta += 1.0;
						G.add(U.get(i));
					}
				} else {
					L.add(U.get(i));
				}
			}
			//TODO: not positive this pointer math works
			if ((partial_sum + partial_sum_delta) -
					pivot_k * (partial_pivot + partial_pivot_delta) < lambda) {
				partial_sum += partial_sum_delta;
				partial_pivot += partial_pivot_delta;
				temp = U;
				U = L;
				L = temp;
			} 
			else {
				temp = U;
				U = G;
				G = temp;
			}
		}

		// Perform the projection.
		float theta = (float)((partial_sum - lambda) / partial_pivot);  
		squaredNorm = 0.0f;
		for (int i = 0; i < dimensions; ++i) {
			if (getValue(i) == 0.0) continue;
			int sign = (getValue(i) > 0) ? 1 : -1;
			if(Float.isNaN(theta)){
				weights[i] = 0;
			} else{
				weights[i] = sign * Math.max((sign * getValue(i) - theta), 0);
			}


			squaredNorm += weights[i] * weights[i];
		}
		scale = 1.0;
	}

	/**
	 * Project this vector into the L1 ball of radius at most lambda, plus or
	 * minus epsilon / 2.
	 * @param lambda radius of ball in L-1 space
	 */ 
	public void projectToL1Ball(float lambda, float epsilon){
		// Re-scale lambda.
		lambda = (float) (lambda / scale);

		// Bail out early if possible.
		float currentL1 = 0.0f;
		float maxValue = 0.0f;
		ArrayList<Float> nonZeros= new ArrayList<Float>();
		for (int i = 0; i < dimensions; ++i) {
			if (weights[i] != 0.0) {
				nonZeros.add(Math.abs(weights[i]));
			} else {
				continue;
			}
			currentL1 += Math.abs(weights[i]);
			if (Math.abs(weights[i]) > maxValue) {
				maxValue = Math.abs(weights[i]);
			}
		}
		if (currentL1 <= (1.0 + epsilon) * lambda) return;

		float min = 0;
		float max = maxValue;
		float theta = 0.0f;
		while (currentL1 >  (1.0 + epsilon) * lambda ||
				currentL1 < lambda) {
			theta = (float) ((max + min) / 2.0);
			currentL1 = 0.0f;
			for (int i = 0; i < nonZeros.size(); ++i) {
				currentL1 += Math.max(0, nonZeros.get(i) - theta);
			}
			if (currentL1 <= lambda) {
				max = theta;
			} else {
				min = theta;
			}
		}

		for (int i = 0; i < dimensions; ++i) {
			if (weights[i] > 0) weights[i] = Math.max(0, weights[i] - theta);
			if (weights[i] < 0) weights[i] = Math.min(0, weights[i] + theta);
		} 
	}

	public double getSquaredNorm(){
		return squaredNorm; 
	}
	public int getDimensions(){
		return dimensions; 
	}

	private void scaleToOne(){
		if(scale!=1.0){
			for (int i = 0; i < dimensions; ++i) {
				weights[i] *= scale;
			}
			scale = 1.0;
		}
	}

	/**
	 * 	 Perform L2 regularization step, penalizing squared norm of
	 * 	 weight vector w.  Note that this regularization step is accomplished
	 * 	 by using w <- (1 - (eta * lambda)) * w, but we use MIN_SCALING_FACTOR
	 * 	 if (1 - (eta * lambda)) < MIN_SCALING_FACTOR to prevent numerical
	 * 	 problems.
	 */
	public void l2Regularize(float eta, float lambda) throws Exception{
		float scalingFactor = 1.0f - (eta * lambda);
		if (scalingFactor > MIN_SCALING_FACTOR) {
			scaleBy(scalingFactor);  
		} else {
			scaleBy(MIN_SCALING_FACTOR); 
		}
	}
	
	/**
	 * 	 Perform L2 regularization step, penalizing squared norm of
	 * 	 weight vector w.  Note that unlike the version that uses a shared
	 *   learning rate, this version needs to touch each coordinate.
	 *   Updates by w_i = w_i - eta * lambda * learningRates[i]
	 *   
	 */
	public void l2Regularize(WeightVector learningRates, float eta, float lambda) throws Exception{
		scaleToOne(); // so we can work directly on the weight vector
		assert learningRates.scale==1;
		float s = eta * lambda;
		for(int i=0; i<dimensions; i++){
			float scalingFactor = (1.0f-learningRates.weights[i]*s);
			if(scalingFactor > MIN_SCALING_FACTOR){
				weights[i] *= scalingFactor;
			} else{
				weights[i] *= MIN_SCALING_FACTOR;
			}
		}
	}
	


	/** Performs L2 regularization penalization for a total of effective_steps
	 *  implied steps.  Does so by using:
	 *  w <- w * ((1 - (eta * lambda)) ^ effective_steps) 
	 */
	public void l2RegularizeSeveralSteps(float eta,
			float lambda,
			float effectiveSteps) throws Exception{
		float scalingFactor = 1.0f - (eta * lambda);
		scalingFactor = (float) Math.pow(scalingFactor, effectiveSteps);
		if (scalingFactor > MIN_SCALING_FACTOR) {
			scaleBy(1.0 - (eta * lambda));  
		} else {
			scaleBy(MIN_SCALING_FACTOR); 
		}
	}
	
	/**
	 * 	 Perform L2 regularization step, penalizing squared norm of
	 * 	 weight vector w.  Note that unlike the version that uses a shared
	 *   learning rate, this version needs to touch each coordinate.
	 *   Updates by w_i = w_i - eta * lambda * learningRates[i]
	 *   
	 */
	public void l2RegularizeSeveralSteps(WeightVector learningRates, float eta, float lambda, float effectiveSteps) throws Exception{
		scaleToOne(); // so we can work directly on the weight vector
		assert learningRates.scale==1;
		float s = eta * lambda;
		for(int i=0; i<dimensions; i++){
			float scalingFactor = (1.0f-learningRates.weights[i]*s);
			scalingFactor = (float) Math.pow(scalingFactor, effectiveSteps);
			//System.out.println(scalingFactor);
			if(scalingFactor > MIN_SCALING_FACTOR){
				weights[i] *= scalingFactor;
			} else{
				weights[i] *= MIN_SCALING_FACTOR;
			}
		}
	}
	

	/**
	 * Makes a new weight vector w=1/(w1*sqrt(w2))
	 * @param w1
	 * @param w2
	 * @return
	 */
	public static WeightVector getInverseSqrtProduct(WeightVector w1, WeightVector w2){
		WeightVector w = new WeightVector(w1.dimensions);
		for(int i=0; i<w1.dimensions; i++){
			if(w1.weights[i]!=0 && w2.weights[i]!= 0)
				w.weights[i] = (float) (1.0/(w1.weights[i] * Math.sqrt(w2.weights[i])));
			else
				w.weights[i] = 0;
		}
		return w;
	}
	/**
	 *  Performs the PEGASOS projection step, projecting w back so that it
	 *  in the feasible set of solutions.
	 * @param lambda regularization amount
	 * @throws Exception if the scaling causes numerical errors
	 */
	public void pegasosProjection(float lambda) throws Exception{
		float projection_val = (float) (1.0f / Math.sqrt(lambda * getSquaredNorm()));
		if (projection_val < 1.0) {
			scaleBy(projection_val);
		}
	}
	
	public float[] toArray(){
		float[] x = new float[dimensions];
		scaleToOne();
		System.arraycopy(weights, 0, x, 0, dimensions);
		return x;
	}
}
