/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;

/**
 * A pairwise sampling approach, this randomly picks one positive and one
 * negative instance, then returns the difference getNextInstance.  
 * Prior to being used, pass a List containing the sampling
 * weights to initializeWeights. They don't need to sum to one, 
 * but they can't change after initialization.
 * 
 * It uses indexed sampling, and does NOT sample by group.
 * @author Mike Scarpati
 */
public class WeightedROCSelector implements InstanceSelector {

	Random r;
	DataSet data;
	boolean initialized;
	ArrayList<Integer> positives;
	ArrayList<Integer> negatives;

	public WeightedROCSelector(){
		r = new Random();
		initialized = false;
	}
	
	@Override
	public void initializeFrom(DataSet data){
		this.data = data;

		// Create index of positives and negatives for fast sampling
		// of disagreeing pairs.
		positives = new ArrayList<Integer>();
		negatives = new ArrayList<Integer>();
		for (int i = 0; i < data.numExamples(); ++i) {
			if (data.vectorAt(i).getY() > 0.0)
				positives.add(i);
			else
				negatives.add(i);
		}
		initialized = true;
	}
	
	public void initializeWeights(List<Float> weights){
		
	}
	/** 
	 * Randomly sample one positive and one negative instance, then return
	 * the difference.  This allows for taking pairwise steps.
	 */
	@Override
	public SparseVector getNextInstance() {
		return new SparseVector(data.vectorAt(positives.get(r.nextInt(positives.size()))),
				data.vectorAt(negatives.get(r.nextInt(negatives.size()))), true);	
	}

	@Override
	public boolean isInitialized() {
		return initialized;
	}

}
