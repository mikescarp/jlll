/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import java.util.ArrayList;
import java.util.Arrays;

import mloss.roc.Curve;

/**
 * TODO: This class was a convenience tool from a while ago, and needs to be cleaned up!
 * @author Mike Scarpati
 *
 */
public class BinaryFit {
	int tp;
	int tn;
	int fp;
	int fn;
	int pp;
	int pn;
	int ap;
	int an;
	int rated;
	
	double sens;
	double spec;
	double acc;
	double thresh;
	double wss;
	double f1;
	double f2;
	double maxPred;
	double minPred;
	double brierScore;
	double mse;

	Curve curve;
	
	/**
	 * This constructor initializes everything to zeros except for
	 * rated and thresh.  Should only be called when we have very few ratings.
	 */
	BinaryFit(int numRated, double thresh){
		tp = fn = fp = tn = 0;
		pp = pn = ap = an = 0;
		rated = numRated;
		this.thresh = thresh;
		sens = spec = acc = wss = f1 = f2 = 0.0f;
		maxPred = Double.MIN_VALUE;
		minPred = Double.MAX_VALUE;
		
		
	}
	
	/*
	 * Takes labels and predictions and threshold then calculates
	 * the statistics we're interested in.
	 * 	
	 * Confusion matrix (row = true, col= predicted):
	 *		                | predict=relevant | predict=irrelevant
	 *		   act=relevant |       tp         |      fn     | sum=ap     
	 *		 act=irrelevant |       fp         |      tn     | sum=an
	 *		                |     sum=pp       |   sum=pn    | sum=rated
	 *		sensitivity: tp/(tp+fn)
	 *		 specificity: tn/(tn+fp)
	 */
	public BinaryFit(ArrayList<Float> trueLabels, ArrayList<Float> predictedLabels, double thresh){
		curve = new Curve.Builder<Float, Float>()
				         .actuals(trueLabels)
				         .predicteds(predictedLabels)
				         .positiveLabel(1.0f).build();
		tp = fn = fp = tn = 0;
		brierScore = mse = 0;
		rated = trueLabels.size();
		this.thresh = thresh;
		for(int i=0; i<trueLabels.size(); i++){
			float pred = predictedLabels.get(i);
			if(trueLabels.get(i)==1.0f){
				if(pred>thresh)
					tp++; //true 0, predict 0
				else
					fn++; //true 0, predict 1
			}
			else{
				if(pred>thresh)
					fp++; //true 1, predict 0
				else
					tn++; //true 1, predict 1
			}
			if(pred>maxPred){
				maxPred = pred;
			} else if(pred<minPred){
				minPred = pred;
			}
			float loss = pred - (trueLabels.get(i)>0 ? 1.0f : 0.0f); 
			brierScore += loss * loss;
			mse += (pred - trueLabels.get(i))*(pred - trueLabels.get(i));
		}
		brierScore /= trueLabels.size();
		mse /= trueLabels.size();
		
		calcFromConfusion();
	}
	
	/**
	 * Sets threshold at sensitivity goal.
	 * TODO: this is a horrible way to handle the difference between sensitivity goal and threshold
	 * @param trueLabels
	 * @param predictedLabels
	 */
	public BinaryFit(ArrayList<Float> trueLabels, ArrayList<Float> predictedLabels, float recallGoal){
		curve = new Curve.Builder<Float, Float>()
				         .actuals(trueLabels)
				         .predicteds(predictedLabels)
				         .positiveLabel(1.0f).build();
		brierScore = mse = 0;
		int[] confusion = curve.confusionMatrix(getThresholdForRecallGoal(recallGoal));
		maxPred = Double.MIN_VALUE;
		minPred = Double.MAX_VALUE;
		tp = confusion[0];
		fp = confusion[1];
		fn = confusion[2];
		tn = confusion[3];
		
		rated = trueLabels.size();
		
		for(int i=0; i<trueLabels.size(); i++){
			float pred = predictedLabels.get(i);
			if(pred>maxPred){
				maxPred = pred;
			} else if(pred<minPred){
				minPred = pred;
			}
			float loss = pred - (trueLabels.get(i)>0 ? 1.0f : 0.0f); 
			brierScore += loss * loss;
			mse += (pred - trueLabels.get(i))*(pred - trueLabels.get(i));
		}
		brierScore /= rated;
		mse /= trueLabels.size();
		this.thresh = -999;
		calcFromConfusion();
	}
	
	/**
	 * Sets threshold around PRBEP
	 * @param trueLabels
	 * @param predictedLabels
	 */
	public BinaryFit(ArrayList<Float> trueLabels, ArrayList<Float> predictedLabels){
		curve = new Curve.Builder<Float, Float>()
				         .actuals(trueLabels)
				         .predicteds(predictedLabels)
				         .positiveLabel(1.0f).build();
		brierScore = mse = 0;
		int[] confusion = curve.confusionMatrix(getPRBEP());
		maxPred = Double.MIN_VALUE;
		minPred = Double.MAX_VALUE;
		tp = confusion[0];
		fp = confusion[1];
		fn = confusion[2];
		tn = confusion[3];
		
		rated = trueLabels.size();
		
		for(int i=0; i<trueLabels.size(); i++){
			float pred = predictedLabels.get(i);
			if(pred>maxPred){
				maxPred = pred;
			} else if(pred<minPred){
				minPred = pred;
			}
			float loss = pred - (trueLabels.get(i)>0 ? 1.0f : 0.0f); 
			brierScore += loss * loss;
			mse += (pred - trueLabels.get(i))*(pred - trueLabels.get(i));
		}
		brierScore /= rated;
		mse /= trueLabels.size();
		this.thresh = -999;
		calcFromConfusion();
	}
	
	private void calcFromConfusion(){
		pp = tp + fp;
		pn = fn + tn;
		ap = tp + fn;
		an = fp + tn;
		
		acc = (double) (tp+tn)/rated;
		// sensitivity and specificity
		// sensitivity: tp/(tp+fn)
		if((tp+fn)==0)
			sens = 0.0f;
		else
			sens = (double) tp/(tp+fn);
		// specificity: tn/(tn+fp)
		if((tn+fp)==0)
			spec = 0.0f;
		else
			spec = (double) tn/(tn+fp);
		
		// WSS, F1, F2
		wss = ((double)(tn+fn)/rated) - 1 + sens;
		double prec = getPrecision();
		double rec = getRecall();
		f1 = 2 * (prec * rec)/(prec + rec);
		f2 = 5 * (prec * rec)/(4*prec + sens);
	}
	
	public int getPRBEP(){
		double[][] prPoints = curve.prPoints();
//		for(int i=0; i< prPoints.length; i++){
//			System.out.print(Arrays.toString(prPoints[i]) + '\t');
//		}
		for(int i=1; i< prPoints.length; i++){
			// each row is [precision, recall]
			if((prPoints[i][0] + 1e-4) >= prPoints[i][1]){
				return i;
			}
		}
		return 0;
	}
	
	public int getThresholdForRecallGoal(float recallGoal){
		double[][] prPoints = curve.prPoints();
//		for(int i=0; i< prPoints.length; i++){
//			System.out.print(Arrays.toString(prPoints[i]) + '\t');
//		}
		for(int i=1; i< prPoints.length; i++){
			// each row is [precision, recall]
			// TODO: verify the sign
			if((recallGoal >= prPoints[i][1])){
				return i;
			}
		}
		return 0;
	}
	
	public double getAvgPrecInRecallRange(float low, float high){
		double[][] prPoints = curve.prPoints();
		double totalPrec = 0.0;
		int cellsInRange = 0;
		for(int i=prPoints.length-1; i>1; i--){
			// each row is [precision, recall]
			
			if((prPoints[i][1]>=low) && (prPoints[i][1] <= high)){
				totalPrec += prPoints[i][0];
				cellsInRange++;
				System.out.println(Arrays.toString(prPoints[i]));
				if(prPoints[i][1] == 1.0){
					break;
				}
			}
		}
		return totalPrec / ((double) cellsInRange);
	}
	
	public String toString() {
		StringBuilder out = new StringBuilder();
		out.append("TP: " + tp + "\t");
		out.append("FP: " + fp + "\t");
		out.append("TN: " + tn + "\t");
		out.append("FN: " + fn + "\n");
		out.append("PP: " + pp + "\t");
		out.append("PN: " + pn + "\t");
		out.append("AP: " + ap + "\t");
		out.append("AN: " + an + "\n");
		out.append("Threshhold: " + thresh + "\t");
		out.append("Sensitivity: " + sens + "\t");
		out.append("Specificity: " + spec + "\n");
		out.append("Accuracy: " + acc + "\t");
		out.append("WSS: " + wss + "\t");
		out.append("F1: " + f1 + "\t");
		out.append("F2: " + f2 + "\n");
		out.append("Max Prediction:" + maxPred + "\t");
		out.append("Min Prediction:" + minPred + "\n");
		out.append("AUROC: " + curve.rocArea() + "\t");
		out.append("AUPR: " + curve.prArea() + "\t");
		out.append("Brier: " + brierScore + "\n");
		out.append("MSE: " + mse + "\t");
		out.append("RMSE: " + Math.sqrt(mse) + "\n");
		out.append("Precision: " + getPrecision() + "\t");
		out.append("Recall: " + getRecall() + "\n");
		return out.toString();
	}
	
	public void add(BinaryFit mf){
		this.tp += mf.tp;
		this.fp += mf.fp;
		this.tn += mf.tn;
		this.fn += mf.fn;
		this.getRated();
		calcFromConfusion();
	}

	public int getTp() {
		return tp;
	}

	public int getTn() {
		return tn;
	}

	public int getFp() {
		return fp;
	}

	public int getFn() {
		return fn;
	}

	public int getPp() {
		return pp;
	}

	public int getPn() {
		return pn;
	}

	public int getAp() {
		return ap;
	}

	public int getAn() {
		return an;
	}

	public int getRated() {
		rated = tp+tn+fp+fn;
		return rated;
	}
	
	public double getPrecision(){
		return ((double) tp / (tp + fp));
	}
	
	public double getRecall(){
		return ((double) tp / (tp + fn));
	}

	public double getSens() {
		return sens;
	}

	public double getSpec() {
		return spec;
	}

	public double getAcc() {
		return acc;
	}

	public double getThresh() {
		return thresh;
	}

	public double getWss() {
		return wss;
	}

	public double getF1() {
		return f1;
	}

	public double getF2() {
		return f2;
	}
	
	public double getAUROC(){
		return curve.rocArea();
	}
	
	public double getAUPR(){
		return curve.prArea();
	}
	
	public double getMAP(int cutoff){
		double prec = 0.0;
		for(int i=1; i<=cutoff; i++){
			prec += curve.precision(i);
		}
		return prec / cutoff;
	}
	
	public double getMSE(){
		return mse;
	}
	
	public double getRMSE(){
		return Math.sqrt(mse);
	}
	
	
	
	public double getBrierScore() {
		return brierScore;
	}
	
}
