/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import com.mikescarpati.jlll.losses.HingeLoss;
import com.mikescarpati.jlll.losses.LossType;

import org.testng.annotations.Test;
import static org.fest.assertions.Assertions.assertThat;

public class HingeLossTest {
	
	@Test
	public void testPredict(){
		LossType loss = new HingeLoss();
		SparseVector x = new SparseVector("1.0 1:1.0 2:-1.0 3:1.5");
		WeightVector w = new WeightVector("0 1 2 3");
		// should be 1*1 + -1*2 + 1.5*3 = 1 - 2 + 4.5 = 3.5
		assertThat(loss.predict(w, x)).isEqualTo(3.5f, StaticTestInfo.delta);
	}
	
	@Test
	public void testLoss(){
		LossType loss = new HingeLoss();
		SparseVector x1 = new SparseVector("1.0 1:1.0 2:-1.0 3:1.5");
		SparseVector x2 = new SparseVector("-1.0 1:1.0 2:-1.0 3:1.5");
		WeightVector w = new WeightVector("0 1 2 3");
		// both predict to 1*1 + -1*2 + 1.5*3 = 1 - 2 + 4.5 = 3.5
		// so x1 should have no loss, x2 s/b 1 + 3.5 = 4.5
		assertThat(loss.getLoss(w, x1)).isEqualTo(0, StaticTestInfo.delta);
		assertThat(loss.getLoss(w, x2)).isEqualTo(4.5f, StaticTestInfo.delta);
	}
	
	@Test
	public void testUpdateDirection(){
		LossType loss = new HingeLoss();
		SparseVector x1 = new SparseVector("1.0 1:1.0 2:-1.0 3:1.5");
		SparseVector x2 = new SparseVector("-1.0 1:1.0 2:-1.0 3:1.5");
		WeightVector w = new WeightVector("0 1 2 3");
		// hinge loss update direction is simply the y value of the vector
		assertThat(loss.getUpdateDirection(w, x1)).isEqualTo(1.0f, StaticTestInfo.delta);
		assertThat(loss.getUpdateDirection(w, x2)).isEqualTo(-1.0f, StaticTestInfo.delta);
	}

}
