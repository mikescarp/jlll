/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.WeightUpdates;

import com.mikescarpati.jlll.SingleFeature;
import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.OptimizationContext;
import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.losses.LossType;

/**
 * SGD with regularization and normalization.  See T. Zhang. Solving large scale linear prediction 
 * problems using stochastic gradient descent algorithms. In ICML �04: Proceedings 
 * of the twenty-first international conference on Machine learning, 2004 for details.
 * 
 * For normalization, see http://arxiv.org/pdf/1305.6646v1.pdf "Normalized online learning"
 * Ross, Miniero, Langford 2013.
 * @author Mike Scarpati
 *
 */
public class NormalizedSgdUpdater implements WeightUpdater {
	WeightVector scales;
	float N;
	
	public NormalizedSgdUpdater(int dimension){
		scales = new WeightVector(dimension);
		N = 0;
	}
	
	private void updateScales(WeightVector w, SparseVector x){
		int i;
		for(SingleFeature feat : x){
			i = feat.index;
			float fv = Math.abs(feat.value); 
			if(fv > scales.getValue(i)){
				// wi <- wi * si^2 / xi^2
				w.setValue(i, w.getValue(i)*scales.getValue(i)*scales.getValue(i)/(fv*fv));
				scales.setValue(i, fv);
			}
		}
	}
	
	/**
	 * Calculates sum xi^2/scalei^2
	 * @param x
	 * @return
	 */
	private float elementwiseQuotientOfSquares(SparseVector x){
		float sum = 0;
		float si;

		for(SingleFeature feat : x){
			if(feat.value != 0){
				si = scales.getValue(feat.index);
				sum += (feat.value * feat.value) / (si*si);
			}
		}
		return sum;
	}

	public WeightVector takeStep(WeightVector w, SparseVector x,
			OptimizationContext oc, LossType p) {
		// Algorithm 1, step 2a
		updateScales(w,x);
		float loss = p.getLoss(w, x);
		N += elementwiseQuotientOfSquares(x);
		try {
			// TODO: Not sure if this is appropriate
			w.l2Regularize(oc.getRate(), oc.getLambda());
			
			if(loss > 0){
				SparseVector scaled = x.elementwiseInverseSquare(scales);			
				w.addVector(scaled, oc.getRate() * (oc.getIteration()/N)* p.getUpdateDirection(w, x));
			}
			return w;
		} catch (Exception e) {
			// Numerical problems
			e.printStackTrace();
		}
		return w;
	}

	public WeightUpdater shallowClone() {
		return new NormalizedSgdUpdater(scales.getDimensions());
	}

}
