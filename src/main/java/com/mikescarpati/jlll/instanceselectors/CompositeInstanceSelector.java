/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import java.util.Random;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;

/**
 * Creates an instance selector from two others and a probability. This will
 * randomly pick between selecting according to the first and second method.
 * This provides a flexible way to do things like CRR from:
 * Sculley, D. "Combined regression and ranking." Proceedings of the 16th 
 * ACM SIGKDD international conference on Knowledge discovery and data mining. 
 * ACM, 2010.
 * 
 * Instead of having stochastic + ROC and stochastic + Rank like sofia-ml, this 
 * architecture would be annoying in a command-line program, but provides additional
 * flexibility when using this code as a library.  For example, one could pair 
 * StochasticSelector and StochasticQueryNormRankSelector to perform something like 
 * CRR while giving equal weight to all queries in the rank step.
 * @author Mike Scarpati
 *
 */
public class CompositeInstanceSelector implements InstanceSelector {

	InstanceSelector selector1, selector2;
	boolean initialized;
	float probOne;
	Random r;
	
	/**
	 * Note: there's no need to have initialized these before sending to this constructor.
	 * @param a
	 * @param b
	 * @param probA
	 */
	public CompositeInstanceSelector(InstanceSelector a, InstanceSelector b, float probA){
		selector1 = a;
		selector2 = b;
		initialized = false;
		probOne = probA;
		r = new Random();
	}
	/**
	 * Uses default parameter of 0.5 for switching
	 * @param a
	 * @param b
	 */
	public CompositeInstanceSelector(InstanceSelector a, InstanceSelector b){
		selector1 = a;
		selector2 = b;
		initialized = false;
		probOne = 0.5f;
		r = new Random();
	}
	
	@Override
	public void initializeFrom(DataSet data) {
		selector1.initializeFrom(data);
		selector2.initializeFrom(data);
		initialized = true;	
	}
	
	/* (non-Javadoc)
	 * @see InstanceSelector#getNextInstance()
	 */
	@Override
	public SparseVector getNextInstance() {
		return (r.nextFloat() < probOne) ? selector1.getNextInstance() : selector2.getNextInstance();
	}

	@Override
	public boolean isInitialized() {
		// TODO Auto-generated method stub
		return initialized;
	}




}
