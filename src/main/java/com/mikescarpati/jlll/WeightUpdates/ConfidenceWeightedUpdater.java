/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.WeightUpdates;

import com.mikescarpati.jlll.OptimizationContext;
import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.losses.LossType;

/**
 * Implements Dredze, Mark, Koby Crammer, and Fernando Pereira. 
 * "Confidence-weighted linear classification." 
 * Proceedings of the 25th international conference on Machine learning. 
 * ACM, 2008.
 * 
 * NOTE: THIS IS NOT CURRENTLY WORKING PROPERLY
 * @author Mike Scarpati
 *
 */
public class ConfidenceWeightedUpdater implements WeightUpdater {

	private final float confidence;
	private WeightVector varianceInverse, variance;
	/**
	 * Creates a new CW updater with initial variance 1.
	 * @param confidence the inverse std normal at the desired probability of correct classification
	 * @param dimension dimension of the weight vector
	 */
	public ConfidenceWeightedUpdater(int dimension, float confidence){
		this.confidence = confidence;
		varianceInverse = new WeightVector(dimension, 1.0f);
		variance = new WeightVector(dimension);
		
	}
	/**
	 * Uses a default of 1.645 for the confidence (corresponding to probability of 95%).
	 */
	public ConfidenceWeightedUpdater(int dimension){
		this.confidence = 1.645f;
		varianceInverse = new WeightVector(dimension, 1.0f);
		variance = new WeightVector(dimension);
	}
	
	public WeightVector takeStep(WeightVector w, SparseVector x,
								 OptimizationContext oc, LossType p) {
		float loss = p.getLoss(w, x);
		if(loss > 0){
			try {
				float margin = x.getY() * p.predict(w, x);
				variance.setAsInverse(varianceInverse);
				SparseVector sigmaX = new SparseVector(variance, x, 1);
				float marginVariance = sigmaX.innerProduct(x);
				float tmp = (1 + 2 * confidence * margin);
				float gamma = (float) ((-tmp + Math.sqrt(tmp * tmp - (8 * confidence * (margin - 
						(confidence * marginVariance))))) / (4 * confidence * marginVariance));
//				System.out.println(String.format("gamma: %.3f, tmp: %.3f, mv: %.3f", gamma, tmp, marginVariance));
				if(gamma > 0){
					
					w.addVector(sigmaX, gamma * p.getUpdateDirection(w, x));
					varianceInverse.addSquaredVector(x, 2 * gamma * confidence);
				}
				
			} catch (Exception e) {
				// Numerical Problems
				e.printStackTrace();
			}
		}
		return w;
	}

	public WeightUpdater shallowClone() {
		// TODO Auto-generated method stub
		return null;
	}

}
