/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import java.util.Random;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;

/**
 * The simplest implementation, this just randomly picks one instance from
 * the dataset and provides it on getNextInstance.
 * @author Mike Scarpati
 */
public class UncertainStochasticSelector implements InstanceSelector {

	Random r;
	boolean initialized;
	DataSet data;
	
	public UncertainStochasticSelector(){
		r = new Random();
		initialized = false;
	}
	
	@Override
	public void initializeFrom(DataSet data){
		this.data = data;
		initialized = true;
	}
	/** 
	 * Randomly picks one instance from the dataset, randomly draws a binary label
	 * according to its probability, then returns it.
	 */
	@Override
	public SparseVector getNextInstance() {
		SparseVector ret = data.vectorAt(r.nextInt(data.numExamples())).clone();
		ret.setY(r.nextFloat()<ret.getY() ? 1.0f : -1.0f);
		return ret;
	}

	@Override
	public boolean isInitialized() {
		// TODO Auto-generated method stub
		return initialized;
	}

}
