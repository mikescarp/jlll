/**
 * 
 */
package com.mikescarpati.jlll;

import static org.fest.assertions.Assertions.assertThat;
import java.io.IOException;
import org.testng.annotations.Test;

/**
 * @author moxienew
 *
 */
public class DataSetTest {

	@Test
	public void testWithBias() throws IOException{
		DataSet ds = new DataSet("src/test/resources/sf-data-set_test.dat", 5, true);
		assertThat(ds.numExamples()).isEqualTo(2);
		assertThat(ds.vectorAt(0).getY()).isEqualTo(1.0f);
		assertThat(ds.vectorAt(0).featureAt(0)).isEqualTo(0);
		assertThat(ds.vectorAt(0).valueAt(0)).isEqualTo(1);
		assertThat(ds.vectorAt(1).getY()).isEqualTo(-1.0f);
	}
	
	@Test
	public void testNoBias() throws IOException{
		DataSet ds = new DataSet("src/test/resources/sf-data-set_test.dat", 5, false);
		assertThat(ds.numExamples()).isEqualTo(2);
		assertThat(ds.vectorAt(0).getY()).isEqualTo(1.0f);
		assertThat(ds.vectorAt(0).featureAt(0)).isEqualTo(0);
		assertThat(ds.vectorAt(0).valueAt(0)).isEqualTo(0);
		assertThat(ds.vectorAt(1).getY()).isEqualTo(-1.0f);
	}
}
