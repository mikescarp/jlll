/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

public class HashWeightVector extends WeightVector {
	int hashMaskBits;
	int hashMask;
	static JenkinsHasher jh = new JenkinsHasher();

	HashWeightVector(int hashMaskBits){
		super(1 << hashMaskBits);
		if (hashMaskBits < 1) {
			throw new IllegalArgumentException("Illegal number of hashMaskBits"
					+ "for of weight vector less than 1: " + hashMaskBits);
		}
		this.hashMaskBits = hashMaskBits;
		this.hashMask = jh.getHashMask(hashMaskBits);

	}

	HashWeightVector(int hashMaskBits, String weightVectorString){
		super(weightVectorString);
		if (hashMaskBits < 1) {
			throw new IllegalArgumentException("Illegal number of hashMaskBits"
					+ "for of weight vector less than 1: " + hashMaskBits);
		}
		this.hashMaskBits = hashMaskBits;
		this.hashMask = jh.getHashMask(hashMaskBits);
	}

	public float innerProduct(SparseVector x, float xScale){
		float inner_product = 0.0f;
		for (int i = 0; i < x.numFeatures(); ++i) {
			inner_product +=
					this.weights[jh.hash(x.featureAt(i), hashMask)] * x.valueAt(i);
		}
		for (int i = 0; i < x.numFeatures(); ++i) {
			float x_i_value = (float) x.valueAt(i);
			int x_i_feature = x.featureAt(i);
			for (int j = i; j < x.numFeatures(); ++j) {
				inner_product +=
						this.weights[jh.hash(x_i_feature, x.featureAt(j), hashMask)] * 
						x_i_value * x.valueAt(j);
			}
		}
		inner_product *= xScale;
		inner_product *= this.scale;
		return inner_product;
	}

	public void addVector(SparseVector x, float xScale) throws Exception {
		float innerProduct = 0.0f;
		float normX = 0.0f;

		for (int i = 0; i < x.numFeatures(); ++i) {
			float this_x_value = (float) (x.valueAt(i) * xScale);
			int this_x_feature = jh.hash(x.featureAt(i), hashMask);
			if (this_x_feature >= this.dimensions) {
				throw new Exception("Error: feature hash id " + this_x_feature +
						" exceeds weight vector dimension " + this.dimensions);
			}
			normX += this_x_value * this_x_value;
			innerProduct += this.weights[this_x_feature] * this_x_value;
			this.weights[this_x_feature] += this_x_value / scale;
		}
		for (int i = 0; i < x.numFeatures(); ++i) {
			float x_i_value = (float) x.valueAt(i);
			int x_i_feature = x.featureAt(i);
			for (int j = i; j < x.numFeatures(); ++j) {
				float this_x_value = (float) (x_i_value * x.valueAt(j) * xScale);
				int this_x_feature = jh.hash(x_i_feature, x.featureAt(j), hashMask);
				if (this_x_feature >= this.dimensions) {
					throw new Exception("Error: cross-product feature hash id " +
										this_x_feature + " exceeds weight vector" +
							" dimension " + this.dimensions);
				}
				normX += this_x_value * this_x_value;
				innerProduct += this.weights[this_x_feature] * this_x_value;
				this.weights[this_x_feature] += this_x_value / this.scale;
			}
		}
		this.squaredNorm += normX + (2.0 * this.scale * innerProduct); 
	}

}
