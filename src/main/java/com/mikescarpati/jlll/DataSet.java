/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Mike Scarpati
 *
 */
public class DataSet extends ArrayList<SparseVector>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2775033460923423849L;
	private boolean useBias;
	

	/**
	 * Create an empty DataSet
	 */
	public DataSet(boolean useBias){
		super();
		this.useBias = useBias;
	}
	
	/**
	 *  Construct and fill a DataSet with data from the given file.
	 * @param fileName location of the file
	 * @param bufferMb size of the buffer used in file reading
	 * @param useBias should a bias term be added?
	 * @throws IOException if there was a problem reading or parsing
	 */
	public DataSet(String fileName, int bufferMb, boolean useBias) throws IOException{
		super();
		this.useBias = useBias;
		BufferedReader in = new BufferedReader(new FileReader(fileName), bufferMb);
    	String line = in.readLine();
    	while(line != null){
    		add(line);
    		line = in.readLine();
    	}
    	in.close();
	}

	public String toString(){
		String out = "";
		for (int i = 0; i < this.size(); ++i) {
		    out += get(i).toString() + "\n";
		}
		return out;
	}
	  
	/**
	 *  Number of total examples in data set.
	 */
	public int numExamples() { return this.size(); }

	/**
	 * Returns a reference to the specified vector.
	 * @param index
	 * @return the SparseVector at the given location
	 */
	public SparseVector vectorAt (int index){ return this.get(index);}

	 /**
	  * Adds the vector represented by this svm-light format string
	  * to the data set.
	  * @param svmLightString
	  */
	public void add(String svmLightString){
		this.add(new SparseVector(svmLightString, useBias));
	}
	  
	/**
	 * Adds a copy of the given vector, using label y.
	 * @param x SparseVector to add
	 * @param y label of the given vector (can differ from the label it holds)
	 */
	public void addLabeledVector(SparseVector x, float y){
		this.add(x);
		this.get(this.size()-1).setY(y);
	}

	/**
	 * Clones a subset of the DataSet
	 */
	public DataSet getSubset(List<Integer> keepIndices){
		DataSet ds = new DataSet(this.useBias);
		for(Integer i : keepIndices){
			ds.add(this.get(i));
		}
		return ds;
	}
	/**
	 * clones the DataSet
	 */
	@Override
	public DataSet clone(){
		DataSet ds = new DataSet(this.useBias);
		for(int i=0; i<this.size(); i++){
			ds.add(this.get(i));
		}
		return ds;
	}
	
	/**
	 * 
	 * @return
	 */
	public DataSet bootstrapSample(){
		DataSet ds = new DataSet(this.useBias);
		Random r = new Random();
		int n = this.size();
		for(int i=0; i<n; i++){
			ds.add(this.get(r.nextInt(n)));
		}
		return ds;
	}
	
	public static DataSet like(DataSet x){
		return new DataSet(x.useBias);
	}
	/**
	 * Gets an ordered ArrayList containing all of the Y values in the DataSet
	 * @return the labels
	 */
	public ArrayList<Float> getAllY(){
		ArrayList<Float> ys = new ArrayList<Float>(numExamples());
		for(int i=0;i<this.size();i++){
			ys.add(this.get(i).getY());
		}
		return ys;
	}
	/**
	 * Unsure when this would be used, but this can set the labels of all
	 * SparseVectors in this DataSet at once.
	 * @param ys the desired labels
	 */
	public void setAllY(ArrayList<Float> ys){
		for(int i=0;i<this.size();i++){
			this.get(i).setY(ys.get(i));
		}
	}
	public boolean isUseBias() {
		return useBias;
	}

	public void setUseBias(boolean useBias) {
		this.useBias = useBias;
	}
	

}
