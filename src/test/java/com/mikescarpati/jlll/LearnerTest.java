/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import com.mikescarpati.jlll.instanceselectors.StochasticSelector;
import com.mikescarpati.jlll.rates.BasicRate;
import com.mikescarpati.jlll.WeightUpdates.SgdUpdater;
import com.mikescarpati.jlll.losses.SquaredLoss;


import org.testng.annotations.Test;

import static org.fest.assertions.Assertions.assertThat;



public class LearnerTest {
	
	

	
	@Test
	public void testTraining(){
		int dim = 10;
		int numObs = 1000;
		// Use all default arguments
		Learner learn = new Learner.Builder(LearnerTestCases.genClassifierData(dim, numObs, 0.001f)).dimensions(dim+1).build();
		learn.train();
		for(int i=1; i<dim; i++){
			assertThat(learn.weightAt(i)).isGreaterThan(0);
		}
		
	}
	
	@Test
	public void testClassificationAccuracy(){
		int dim = 10;
		int numObs = 1000;
		// Use all default arguments
		ILearner learn = new Learner.Builder(LearnerTestCases.genClassifierData(dim, numObs, 0.001f))
		                           .dimensions(dim+1)
		                           .build();
		learn.train();
		DataSet testData = LearnerTestCases.genClassifierData(dim, 500, 0.001f);
		BinaryFit fit = new BinaryFit(testData.getAllY(), learn.predict(testData));
		assertThat(fit.getAcc()).isGreaterThan(0.95f);
	}
	
	@Test
	public void testRegressionPerformance(){
		int dim = 10;
		int numObs = 1000;
		// Use all default arguments
		ILearner learn = new Learner.Builder(LearnerTestCases.genClassifierData(dim, numObs, 0.001f))
		                           .dimensions(dim+1)
		                           .rateMultiplier(0.01f)
		                           .selector(new StochasticSelector())
		                           .updater(new SgdUpdater())
		                           .rate(new BasicRate())
		                           .loss(new SquaredLoss())
		                           .build();
		learn.train();
		DataSet testData = LearnerTestCases.genClassifierData(dim, 500, 0.001f);
		
		RegressionFit fit = new RegressionFit(testData.getAllY(), learn.predict(testData));
		System.out.println(learn.predict(testData).toString());
		assertThat(fit.getMAE()).isLessThan(0.1f);
	}
	
	@Test(dataProviderClass=LearnerTestCases.class, dataProvider="regressionLearners")
	public void testAllRegression(ILearner learn){
		DataSet testData = LearnerTestCases.genClassifierData(10, 500, 0.001f);
		learn.train();
		RegressionFit fit = new RegressionFit(testData.getAllY(), learn.predict(testData));
		assertThat(fit.getMAE()).isLessThan(0.1f);
	}

	@Test(dataProviderClass=LearnerTestCases.class, dataProvider="logisticLearners")
	public void testAllLogReg(ILearner learn){
		DataSet testData = LearnerTestCases.genClassifierData(10, 500, 0.001f);
		learn.train();
		BinaryFit fit = new BinaryFit(testData.getAllY(), learn.predict(testData));
		assertThat(fit.getAcc()).isGreaterThan(0.95f);
		assertThat(fit.getAUROC()).isGreaterThan(0.95f);
	}
	
	@Test(dataProviderClass=LearnerTestCases.class, dataProvider="hingeLearners")
	public void testAllHingeLearners(ILearner learn){
		DataSet testData = LearnerTestCases.genClassifierData(10, 500, 0.001f);
		learn.train();
		BinaryFit fit = new BinaryFit(testData.getAllY(), learn.predict(testData));
		assertThat(fit.getAcc()).isGreaterThan(0.95f);
		assertThat(fit.getAUROC()).isGreaterThan(0.95f);
	}
}
