/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.losses;

import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.SparseVector;

/**
 * Like logistic regression, but input should be between 0 and 1.
 * Supports decimal labels, with the interpretation that they are
 * partially positive, partially negative
 * @author Mike Scarpati
 *
 */
public class Soft01LogRegLoss extends LossType {
	
	@Override
	public float predict(WeightVector w, SparseVector x) {
		float p = w.innerProduct(x);
		return (float) (1 / (1.0 + Math.exp(-p)));
	}

	/**
	 * Likelihood: p^y * (1-p)^(1-y)
	 */
	@Override
	public float getLoss(WeightVector w, SparseVector x) {
		float p = (float) (1.0 / (1.0 + Math.exp(-w.innerProduct(x)))); 
		return (float) (Math.pow(p, x.getY()) * Math.pow(1-p, 1-x.getY()));
	}

	/**
	 * y - 1/(1+exp(-<w,x>))
	 */
	@Override
	public float getUpdateDirection(WeightVector w, SparseVector x) {
		return (float) (x.getY() - (1.0 / (1 + Math.exp(-x.getY() * w.innerProduct(x)))));
	}
}
