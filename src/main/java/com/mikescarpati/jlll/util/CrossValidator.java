package com.mikescarpati.jlll.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.BinaryFit;
import com.mikescarpati.jlll.ILearner;

public class CrossValidator {
	
	int folds;
	ILearner learner;
	
	public CrossValidator(int folds, ILearner learner){
		this.folds = folds;
		this.learner = learner;
	}
	public BinaryFit crossValidationResults() throws Exception{
		ArrayList<Float> preds;
		//		float thresh = findThreshold(0.0f, false, nFolds);
		preds = crossValidationPredictions();
		return new BinaryFit(learner.getDataSet().getAllY(), preds);
	}
	public BinaryFit crossValidationResults(float thresh) throws Exception{
		ArrayList<Float> preds;
		//		float thresh = findThreshold(0.0f, false, nFolds);
		preds = crossValidationPredictions();
		return new BinaryFit(learner.getDataSet().getAllY(), preds, (double)thresh);
	}
	
	public BinaryFit crossValidationResultsAtRecallGoal(float recallGoal) throws Exception{
		ArrayList<Float> preds;
		//		float thresh = findThreshold(0.0f, false, nFolds);
		preds = crossValidationPredictions();
		return new BinaryFit(learner.getDataSet().getAllY(), preds, recallGoal);
	}
	/*
	 * 
	 */
	public ArrayList<Float> crossValidationPredictions() throws Exception{
		ArrayList<Float> preds = new ArrayList<Float>();
		ArrayList<Integer> indices = new ArrayList<Integer>();
		ILearner tmpLearner;
		DataSet testSet;
		DataSet trainSet;
		int nObs = learner.getDataSet().size();
		// get random permutation of the indices
		for(int i=0; i<nObs; i++){
			indices.add(i);
		}
		Collections.shuffle(indices);
		// take a partition, train, predict
		for(int i=0;i<folds;i++){
			int testStart = i * nObs / folds;
			int testEnd = (i+1) * nObs / folds;
			testSet = new DataSet(learner.getDataSet().isUseBias());
			trainSet = new DataSet(learner.getDataSet().isUseBias());
			for(int j=testStart; j<testEnd; j++){ //populate test set
				testSet.add(learner.getDataSet().get(indices.get(j)));
			}
			for(int j=0; j<testStart; j++){ // add train before test
				trainSet.add(learner.getDataSet().get(indices.get(j)));
			}
			for(int j=testEnd; j<indices.size(); j++){ // add train after test
				trainSet.add(learner.getDataSet().get(indices.get(j)));
			}
			tmpLearner = learner.shallowClone(trainSet);
			tmpLearner.setDataSet(trainSet);
			tmpLearner.train();
			preds.addAll(tmpLearner.predict(testSet));			
		}
		Float[] ordPreds = new Float[preds.size()];
		for(int i=0; i<preds.size(); i++){
			ordPreds[indices.get(i)] = preds.get(i);
		}
		return new ArrayList<Float>(Arrays.asList(ordPreds));
	}
	
	public ArrayList<Float> LOOCVPredictions() throws Exception{
		ArrayList<Float> preds = new ArrayList<Float>();
		//ArrayList<Integer> indices = new ArrayList<Integer>();
		ILearner tmpLearner;
		DataSet trainSet;
		
		for(int i=0;i<learner.getDataSet().size();i++){
			trainSet = learner.getDataSet().clone();
			trainSet.remove(i);
			
			tmpLearner = learner.shallowClone(trainSet);
			tmpLearner.setDataSet(trainSet);
			tmpLearner.train();
			preds.add(tmpLearner.predictOne(learner.getDataSet().get(i)));			
		}
		return preds;
	}
}
