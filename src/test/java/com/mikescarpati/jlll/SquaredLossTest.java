/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import static org.fest.assertions.Assertions.assertThat;

import com.mikescarpati.jlll.losses.LossType;
import com.mikescarpati.jlll.losses.SquaredLoss;

import org.testng.annotations.Test;

public class SquaredLossTest {
	@Test
	public void testPredict(){
		LossType loss = new SquaredLoss();
		SparseVector x = new SparseVector("1.0 1:1.0 2:-1.0 3:1.5");
		WeightVector w = new WeightVector("0 1 2 3");
		// should be 1*1 + -1*2 + 1.5*3 = 1 - 2 + 4.5 = 3.5
		assertThat(loss.predict(w, x)).isEqualTo(3.5f, StaticTestInfo.delta);
	}
	
	@Test
	public void testLoss(){
		LossType loss = new SquaredLoss();
		SparseVector x1 = new SparseVector("2.5 1:1.0 2:-1.0 3:1.5");
		SparseVector x2 = new SparseVector("-0.5 1:1.0 2:-1.0 3:1.5");
		WeightVector w = new WeightVector("0 1 2 3");
		// both predict to 1*1 + -1*2 + 1.5*3 = 1 - 2 + 4.5 = 3.5
		// so x1 should have (3.5-2.5)^2=1, x2 s/b (3.5- -.5)^2=16
		assertThat(loss.getLoss(w, x1)).isEqualTo(1, StaticTestInfo.delta);
		assertThat(loss.getLoss(w, x2)).isEqualTo(16, StaticTestInfo.delta);
	}
	
	@Test
	public void testUpdateDirection(){
		LossType loss = new SquaredLoss();
		SparseVector x1 = new SparseVector("2.5 1:1.0 2:-1.0 3:1.5");
		SparseVector x2 = new SparseVector("-0.5 1:1.0 2:-1.0 3:1.5");
		WeightVector w = new WeightVector("0 1 2 3");
		// squared loss update direction is simply the y value - prediction
		assertThat(loss.getUpdateDirection(w, x1)).isEqualTo(-1, StaticTestInfo.delta);
		assertThat(loss.getUpdateDirection(w, x2)).isEqualTo(-4, StaticTestInfo.delta);
	}
}
