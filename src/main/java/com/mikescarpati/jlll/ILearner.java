package com.mikescarpati.jlll;

import java.util.ArrayList;

public interface ILearner {

	/**
	 * The Learner will have had a DataSet provided during construction.  This 
	 * runs the learning process, so that predictions or evaluations can be made. 
	 */
	void train();

	ArrayList<Float> predict(DataSet testData);

	ArrayList<Float> predict();

	float predictOne(SparseVector input);

	float objectiveValue(DataSet testData);

	ILearner shallowClone(DataSet x);
	/**
	 * This provides an verbose description of the options used by this Learner.
	 */
	String toString();

	/**
	 * This provides a shorter description of the options used by this Learner.
	 */
	String shortString();

	DataSet getDataSet();

	void setDataSet(DataSet trainSet);

}