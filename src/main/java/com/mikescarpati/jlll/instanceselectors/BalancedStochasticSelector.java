/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import java.util.ArrayList;
import java.util.Random;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.SparseVector;

/**
 * This will randomly pick one positive instance, then one negative instance.
 * When this is used, y should be in {-1, +1}.  It uses indexed sampling
 * for faster picking of positives and negatives.  This can be useful on
 * imbalanced datasets, but I've had better luck with the pairwise methods.
 * Note that this will be faster than those, because this doens't have to take
 * a SparseVector difference each step.
 * @author Mike Scarpati
 */
public class BalancedStochasticSelector implements InstanceSelector {

	Random r;
	DataSet data;
	boolean initialized;
	ArrayList<Integer> positives;
	ArrayList<Integer> negatives;
	boolean lastPositive; //keeps track of state

	public BalancedStochasticSelector(){
		r = new Random();
		initialized = false;
	}
	
	@Override
	public void initializeFrom(DataSet data){
		this.data = data;

		// Create index of positives and negatives for fast sampling
		// of disagreeing pairs.
		positives = new ArrayList<Integer>();
		negatives = new ArrayList<Integer>();
		for (int i = 0; i < data.numExamples(); ++i) {
			if (data.vectorAt(i).getY() > 0.0)
				positives.add(i);
			else
				negatives.add(i);
		}
		lastPositive = true;
		initialized = true;
	}
	/** 
	 * Randomly picks one instance from the positive or negative part of 
	 * the dataset depending on which class was last returned.
	 */
	@Override
	public SparseVector getNextInstance() {
		lastPositive = !lastPositive;
		if(lastPositive){
			return data.vectorAt(positives.get(r.nextInt(positives.size())));			
		}else{
			return data.vectorAt(negatives.get(r.nextInt(negatives.size())));
		}
		
	}

	@Override
	public boolean isInitialized() {
		// TODO Auto-generated method stub
		return initialized;
	}

}
