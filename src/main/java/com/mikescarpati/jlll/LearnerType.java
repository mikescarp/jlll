/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

public enum LearnerType {
	PEGASOS,  // Pegasos SVM, using lambda as regularization parameter
    MARGIN_PERCEPTRON,  // Perceptron with Margins, where margin size is determined by parameter c
    PASSIVE_AGGRESSIVE,  // Passive Aggressive Perceptron, where c controls the maximum step size.
    LOGREG_PEGASOS,  // Logistic Regression using Pegasos projection, and lambda as regularization parameter.
    LOGREG,  // Logistic Regression using lambda as regularization parameter.
    LMS_REGRESSION, // Least-mean-squares Regression (using Pegasos projection), and lambda
                    //as regularization parameter.
    SGD_SVM,  // Stochastic Gradient Descent SVM; lambda is regularization parameter.
    ROMMA  // ROMMA algorithm; takes no regularization parameter.
}
