/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import java.util.Random;

import com.mikescarpati.jlll.WeightUpdates.PassiveAggressiveOneUpdater;
import com.mikescarpati.jlll.WeightUpdates.PegasosUpdater;
import com.mikescarpati.jlll.WeightUpdates.WeightUpdater;
import com.mikescarpati.jlll.losses.LogRegLoss;
import com.mikescarpati.jlll.losses.SquaredLoss;
import com.mikescarpati.jlll.rates.BasicRate;
import com.mikescarpati.jlll.rates.ConstantRate;
import com.mikescarpati.jlll.rates.LearningRate;
import com.mikescarpati.jlll.rates.PegasosRate;
import com.mikescarpati.jlll.WeightUpdates.ConfidenceWeightedUpdater;
import com.mikescarpati.jlll.WeightUpdates.PassiveAggressiveTwoUpdater;
import com.mikescarpati.jlll.WeightUpdates.SgdUpdater;
import com.mikescarpati.jlll.instanceselectors.BalancedStochasticSelector;
import com.mikescarpati.jlll.instanceselectors.CompositeInstanceSelector;
import com.mikescarpati.jlll.instanceselectors.InstanceSelector;
import com.mikescarpati.jlll.instanceselectors.StochasticQueryNormRankSelector;
import com.mikescarpati.jlll.instanceselectors.StochasticROCSelector;
import com.mikescarpati.jlll.instanceselectors.StochasticRankSelector;
import com.mikescarpati.jlll.instanceselectors.StochasticSelector;
import com.mikescarpati.jlll.losses.HingeLoss;

import org.testng.annotations.DataProvider;

public class LearnerTestCases {

	/**
	 * Helper for generating a random DataSet.
	 * @param dim Number of dimensions to generate.
	 * @param centroid value in each dimension where the center of the class is
	 * @param yVal is the y value of the new instance
	 * @param variance how much variance should we have in each dimension?
	 * @return the randomly generated SparseVector
	 */
	public static SparseVector genRandomVector(int dim, float centroid, float yVal, float variance){
		Random r = new Random();
		float[] vals = new float[dim];
		
		for(int i=0; i<dim; i++){
			vals[i] = centroid + ((float) r.nextGaussian() * variance);
		}
		return new SparseVector(vals, yVal, false);
	}
	/**
	 * Creates a dataset with two classes: a positive class with 1's in every
	 * dimension, and a negative class with -1's in every dimension. The 
	 * individual data points are generated from a random normal with mean +/- 1.
	 * We put all positive then all negative to make sure randomness works when
	 * selecting instances to learn from.
	 * @param dim number of dimensions to put in the data
	 * @param numObs how many observations should we make?
	 * @param variance how much variance should we have in each dimension?
	 * @return the simulated DataSet
	 */
	public static DataSet genClassifierData(int dim, int numObs, float variance){
		DataSet ds = new DataSet(false);
		int i = 0;
		while(i++ < ((numObs+1)/2)){
			ds.add(genRandomVector(dim, 1.0f, 1.0f, variance));
		}
		while(i++ < numObs){
			ds.add(genRandomVector(dim, -1.0f, -1.0f, variance));
		}
		return ds;
	}
	
	@DataProvider(name = "regressionLearners")
	public static Object[][] createRegressionLearners() {
		DataSet data = genClassifierData(10, 1000, 0.01f);
		//DataSet testData = genClassifierData(10, 500, 0.01f);
		InstanceSelector[] selectors = {
				new BalancedStochasticSelector(),
				new StochasticSelector()
		};
		WeightUpdater[] updaters = {
				new PassiveAggressiveOneUpdater(1.0f),
				new PassiveAggressiveTwoUpdater(1.0f),
				new PegasosUpdater(),
				new SgdUpdater()
		};
		LearningRate[] rates = {
				new BasicRate(),
				new ConstantRate(),
				new PegasosRate(0.1f)
		};
		int idx=0;
		Object[][] ret = new Object[selectors.length * updaters.length * rates.length][1];
		for(int i=0; i<selectors.length; i++){
			for(int j=0; j<updaters.length; j++){
				for(int k=0; k<rates.length; k++){
					ret[idx++][0] = new Learner.Builder(data)
							                   .dimensions(10+1)
							                   .rateMultiplier(0.2f)
							                   .selector(selectors[i])
							                   .updater(updaters[j])
							                   .rate(rates[k])
							                   .loss(new SquaredLoss())
							                   .build();
					
				}
			}
		}
	 return ret;
	}
	
	@DataProvider(name = "logisticLearners")
	public static Object[][] createLogisticLearners() {
		int dim = 10;
		DataSet data = genClassifierData(dim, 1000, 0.01f);
		//DataSet testData = genClassifierData(10, 500, 0.01f);
		InstanceSelector[] selectors = {
				new BalancedStochasticSelector(),
				new StochasticSelector(),
				new StochasticQueryNormRankSelector(),
				new StochasticRankSelector(),
				new StochasticROCSelector(),
				new CompositeInstanceSelector(new StochasticSelector(), new StochasticROCSelector(), 0.5f)
		};
		WeightUpdater[] updaters = {
				new PassiveAggressiveOneUpdater(1.0f),
				new PassiveAggressiveTwoUpdater(1.0f),
				new PegasosUpdater(),
				new SgdUpdater(),
				new ConfidenceWeightedUpdater(dim + 1)
		};
		LearningRate[] rates = {
				new BasicRate(),
				new ConstantRate(),
				new PegasosRate(0.1f)
		};
		
		int idx=0;
		Object[][] ret = new Object[selectors.length * updaters.length * rates.length][1];
		for(int i=0; i<selectors.length; i++){
			for(int j=0; j<updaters.length; j++){
				for(int k=0; k<rates.length; k++){
					ret[idx++][0] = new Learner.Builder(data)
							                   .dimensions(dim+1)
							                   .selector(selectors[i])
							                   .updater(updaters[j])
							                   .rate(rates[k])
							                   .loss(new LogRegLoss())
							                   .build();
					
				}
			}
		}
	 return ret;
	}
	
	@DataProvider(name = "hingeLearners")
	public static Object[][] createHingeLearners() {
		int dim = 10;
		DataSet data = genClassifierData(dim, 1000, 0.01f);
		//DataSet testData = genClassifierData(10, 500, 0.01f);
		InstanceSelector[] selectors = {
				new BalancedStochasticSelector(),
				new StochasticSelector(),
				new StochasticQueryNormRankSelector(),
				new StochasticRankSelector(),
				new StochasticROCSelector(),
				new CompositeInstanceSelector(new StochasticSelector(), new StochasticROCSelector(), 0.5f)
		};
		WeightUpdater[] updaters = {
				new PassiveAggressiveOneUpdater(1.0f),
				new PassiveAggressiveTwoUpdater(1.0f),
				new PegasosUpdater(),
				new SgdUpdater()
		};
		LearningRate[] rates = {
				new BasicRate(),
				new ConstantRate(),
				new PegasosRate(0.1f)
		};
		
		int idx=0;
		Object[][] ret = new Object[selectors.length * updaters.length * rates.length][1];
		for(int i=0; i<selectors.length; i++){
			for(int j=0; j<updaters.length; j++){
				for(int k=0; k<rates.length; k++){
					ret[idx++][0] = new Learner.Builder(data)
							                   .dimensions(dim+1)
							                   .selector(selectors[i])
							                   .updater(updaters[j])
							                   .rate(rates[k])
							                   .loss(new HingeLoss())
							                   .build();
					
				}
			}
		}
	 return ret;
	}
}
