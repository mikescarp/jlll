/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.rates;

/**
 * Rate doesn't change with number of iterations. Default is 0.02
 */
public class ConstantRate implements LearningRate {
	int i;
	
	public ConstantRate(){
		i=1;
	}
	@Override
	public float getRate() {
		return 0.02f;
	}

	@Override
	public void increment() {
		i++;
	}

	@Override
	public void reset() {
		i=1;
	}

	@Override
	public int getIteration() {
		// TODO Auto-generated method stub
		return i;
	}

}
