package com.mikescarpati.jlll.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class WeightedSampler {
	
	private class IndexNode implements Comparable<IndexNode>{
		public float val;
		public int index;
		
		public IndexNode(int index, float val){
			this.index = index;
			this.val = val;
		}

		@Override
		public int compareTo(IndexNode x) {
			return Float.compare(val, x.val);
		}
	}
	
	IndexNode[] data;
	float[] eCDF;
	Random r;
	
	public WeightedSampler(List<Float> weights){
		data = new IndexNode[weights.size()];
		int i=0;
		for(float x : weights){
			data[i] = new IndexNode(i++, x);
		}
		Arrays.sort(data, Collections.reverseOrder());
		eCDF = new float[data.length];
		eCDF[0] = data[0].val;
		for(int j=1; j<data.length; j++){
			eCDF[j] = eCDF[j-1] + data[j].val;
		}
		r = new Random();
	}
	
	public int nextIndex(){
		float f = r.nextFloat();
		return data[Math.abs(Arrays.binarySearch(eCDF, f))-1].index;
	}

}
