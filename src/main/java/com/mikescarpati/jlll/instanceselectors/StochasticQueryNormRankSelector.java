/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.MutableInt;
import com.mikescarpati.jlll.SparseVector;

/**
 * This first randomly selects a query, giving equal weight to all queries 
 * regardless of number of examples.  It then randomly picks another instance 
 * with a different Y-value but in the same query, returning the difference
 * on getNextInstance.
 * 
 * It uses indexed sampling for faster picking of positives and negatives.
 * @author Mike Scarpati
 */
public class StochasticQueryNormRankSelector implements InstanceSelector {

	Random r;
	DataSet data;
	boolean initialized;
	int maxTries; //TODO: figure out a way to set this?
	// groupId      y-val           index in DataSet
	Map<String, Map<Float,ArrayList<Integer>>> groupIdYToIndex;
	// groupId   numberPresent
	Map<String, MutableInt> groupIdYToCount;
	String[] groupIds;

	
	public StochasticQueryNormRankSelector(){
		maxTries = 500;
		r = new Random();
		initialized = false;
	}
	
	@Override
	public void initializeFrom(DataSet data){
		this.data = data;

		groupIdYToIndex = new HashMap<String, Map<Float,ArrayList<Integer>>>(data.numExamples());
		groupIdYToCount = new HashMap<String, MutableInt>(data.numExamples());

		for (int i = 0; i < data.numExamples(); ++i) {
			String group_id = data.vectorAt(i).getGroupId();
			// make sure we have the map for group id to ids by rank
			if(!groupIdYToIndex.containsKey(group_id)){
				// note: we iterate over this in the main loop, so LinkedHashMap should be faster
				groupIdYToIndex.put(group_id, new LinkedHashMap<Float,ArrayList<Integer>>());
			}
			// make sure the rank -> ids map exists
			if(!groupIdYToIndex.get(group_id).containsKey(data.vectorAt(i).getY())){
				groupIdYToIndex.get(group_id).put(data.vectorAt(i).getY(), new ArrayList<Integer>());
			}
			groupIdYToIndex.get(group_id).get(data.vectorAt(i).getY()).add(i);
			MutableInt count = groupIdYToCount.get(group_id);
			if (count == null) {
				groupIdYToCount.put(group_id, new MutableInt());
			}
			else {
				count.increment();
			}
		}
		groupIds = groupIdYToIndex.keySet().toArray(new String[groupIdYToIndex.keySet().size()]);
		initialized = true;
	}
	/** 
	 * Randomly sample a group (query), then pick a random instance.  Return
	 * the difference between that and another randomly chosen instance in the
	 * same group with a different Y value. Unlike the original sofia-ml code,
	 * this uses indexed sampling which should be faster on imbalanced data.
	 */
	@Override
	public SparseVector getNextInstance() {
		int range = 0; // number of instances in our group with diff y-val
		int tries = 0;
		SparseVector a = null;
		SparseVector b = null;
		String groupId;
		float aY = 0f;
		Map<Float,ArrayList<Integer>> yToList = null;
		
		while(range == 0 && tries < maxTries){
			// first pick a random group, then randomly within that group
			groupId = groupIds[r.nextInt(groupIds.length)];
			int randomExample = r.nextInt(groupIdYToCount.get(groupId).get());
			a = data.vectorAt(randomExample);
			aY = a.getY();
			// Limit our query to instances in our group_id
			yToList = groupIdYToIndex.get(groupId);
			// How many in this group have a different y value?
			range = groupIdYToCount.get(groupId).get() - 
					groupIdYToIndex.get(groupId).get(aY).size();
			tries++;
		}
		if(range == 0 || tries == maxTries){
			return null;
		}

		// Pick randomly from the remaining
		int bLocation = r.nextInt(range);
		for (Map.Entry<Float, ArrayList<Integer>> entry: yToList.entrySet()) {
			if (entry.getKey() == aY) continue; // skip if same y value
			if (bLocation < entry.getValue().size()) {
				b = data.vectorAt((entry.getValue().get(bLocation)));
				break;
			}
			bLocation -= entry.getValue().size();
		}
		return new SparseVector(a, b, true);		
	}

	@Override
	public boolean isInitialized() {
		// TODO Auto-generated method stub
		return initialized;
	}

}
