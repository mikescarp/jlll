package com.mikescarpati.jlll;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class BaggedLearner implements ILearner {
	List<Learner> learners;
	
	public BaggedLearner(Learner learn, int numBags) {
		learners = new LinkedList<Learner>();
		for(int i=0; i<numBags; i++){
			//learners.add(learn.shallowClone(learn.data));
			learners.add(learn.shallowClone(learn.data.bootstrapSample()));
		}
	}

	@Override
	public void train() {
		for(Learner learn : learners){
			learn.train();
		}		
	}

	@Override
	public ArrayList<Float> predict(DataSet testData) {
		Float[] preds = new Float[testData.size()];
		Arrays.fill(preds, 0f);
		
		for(Learner learn : learners){
			int i = 0;
			for(Float f : learn.predict(testData)){
				preds[i++] += f;
			}
		}
		return new ArrayList<Float>(Arrays.asList(preds));
	}

	@Override
	public ArrayList<Float> predict() {
		return predict(learners.get(0).data);
	}

	@Override
	public float predictOne(SparseVector input) {
		float pred = 0;
		for(Learner learn : learners){
			pred += learn.predictOne(input);
		}
		return pred;
	}

	/**
	 * This doesn't work right now
	 */
	@Override
	public float objectiveValue(DataSet testData) {
		return 0;
	}

	@Override
	public String shortString() {
		return String.format("Bagged %s of %s", learners.size(), learners.get(0).shortString());
	}
	
	@Override
	public String toString() {
		return String.format("Bagged %s of %s", learners.size(), learners.get(0).toString());
	}

	@Override
	public DataSet getDataSet() {
		return learners.get(0).data;
	}

	@Override
	public ILearner shallowClone(DataSet x) {
		Learner ret = new Learner();
		Learner basis = learners.get(0);
		ret.selector = basis.selector;
		ret.selector.initializeFrom(x);
		ret.data = x;
		ret.context = new OptimizationContext(basis.context.lambda, basis.context.rate);
		ret.context.setRateMultiplier(basis.context.rateMultiplier);
		ret.updater = basis.updater.shallowClone();
		ret.weights = new WeightVector(basis.weights.dimensions);
		ret.loss = basis.loss;
		ret.dimension = basis.dimension;
		ret.iterations = basis.iterations;
		ret.projectL1every = basis.projectL1every;
		ret.projectL1lambda = basis.projectL1lambda;
		ret.projectL1epsilon = basis.projectL1epsilon;
		
		return new BaggedLearner(ret, learners.size());
	}

	@Override
	public void setDataSet(DataSet trainSet) {
		for(Learner learn : learners){
			learn.setDataSet(trainSet);
		}
		
	}

}
