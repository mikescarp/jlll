/**
 * 
 */
package com.mikescarpati.jlll.instanceselectors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import com.mikescarpati.jlll.DataSet;
import com.mikescarpati.jlll.MutableInt;
import com.mikescarpati.jlll.SparseVector;

/**
 * A pairwise sampling approach, this randomly picks one instance, then
 * randomly picks another with a different rank from the same query.  The
 * difference is returned on getNextInstance.  It uses indexed sampling.
 * @author Mike Scarpati
 */
public class StochasticRankSelector implements InstanceSelector {

	Random r;
	DataSet data;
	boolean initialized;
	int maxTries; //TODO: make accessible
	// groupId      y-val           index in DataSet
	Map<String, Map<Float,ArrayList<Integer>>> groupIdYToIndex;
	// groupId   numberPresent
	Map<String, MutableInt> groupIdYToCount;

	public StochasticRankSelector(){
		r = new Random();
		maxTries = 500;
		initialized = false;
	}
	@Override
	public void initializeFrom(DataSet data){
		
		this.data = data;

		groupIdYToIndex = new HashMap<String, Map<Float,ArrayList<Integer>>>(data.numExamples());
		groupIdYToCount = new HashMap<String, MutableInt>(data.numExamples());

		for (int i = 0; i < data.numExamples(); ++i) {
			String group_id = data.vectorAt(i).getGroupId();
			// make sure we have the map for group id to ids by rank
			if(!groupIdYToIndex.containsKey(group_id)){
				// note: we iterate over this in the main loop, so LinkedHashMap should be faster
				groupIdYToIndex.put(group_id, new LinkedHashMap<Float,ArrayList<Integer>>());
			}
			// make sure the rank -> ids map exists
			if(!groupIdYToIndex.get(group_id).containsKey(data.vectorAt(i).getY())){
				groupIdYToIndex.get(group_id).put(data.vectorAt(i).getY(), new ArrayList<Integer>());
			}
			groupIdYToIndex.get(group_id).get(data.vectorAt(i).getY()).add(i);
			MutableInt count = groupIdYToCount.get(group_id);
			if (count == null) {
				groupIdYToCount.put(group_id, new MutableInt());
			}
			else {
				count.increment();
			}
		}
		initialized = true;
	}
	
	/** 
	 * Randomly sample one an instance, then pick another from the same group
	 * with a different y value, returning the difference.  
	 * This allows for taking pairwise steps.
	 */
	@Override
	public SparseVector getNextInstance() {
		
		int range = 0; // number of instances in our group with diff y-val
		int tries = 0;
		SparseVector a = null;
		SparseVector b = null;
		String groupId;
		float aY = 0f;
		Map<Float,ArrayList<Integer>> yToList = null;
		
		while(range == 0 && tries < maxTries){
			// first can be picked purely at random, but needs to have
			// some instances with other ranks in its group
			int randomExample = r.nextInt(data.numExamples());
			a = data.vectorAt(randomExample);
			groupId = a.getGroupId();
			aY = a.getY();
			// Limit our query to instances in our group_id
			yToList = groupIdYToIndex.get(groupId);
			// How many in this group have a different y value?
			range = groupIdYToCount.get(groupId).get() - 
					groupIdYToIndex.get(groupId).get(aY).size();
			tries++;
		}
		if(range == 0 || tries == maxTries){
			return null;
		}

		// Pick randomly from the remaining
		int bLocation = r.nextInt(range);
		for (Map.Entry<Float, ArrayList<Integer>> entry: yToList.entrySet()) {
			if (entry.getKey() == aY) continue; // skip if same y value
			if (bLocation < entry.getValue().size()) {
				b = data.vectorAt((entry.getValue().get(bLocation)));
				break;
			}
			bLocation -= entry.getValue().size();
		}
		return new SparseVector(a, b, true);	
	}
	@Override
	public boolean isInitialized() {
		return initialized;
	}

}
