/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.losses;

import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.SparseVector;

/**
 * Lets us handle regression problems.
 * @author Mike Scarpati
 *
 */
public class ClampedSVRLoss extends LossType {
	float min;
	float max;
	float epsilon; // don't move if within this of true
	
	public ClampedSVRLoss(float min, float max, float epsilon){
		this.min = min;
		this.max = max;
		this.epsilon = epsilon;
	}
	@Override
	public float predict(WeightVector w, SparseVector x) {
		return clamp(w.innerProduct(x));
	}

	private float clamp(float x){
		if(x<min)
			return min;
		if(x>max)
			return max;
		return x;					
	}
	@Override
	public float getLoss(WeightVector w, SparseVector x) {

		float pred = clamp(w.innerProduct(x));
		float err = Math.abs(x.getY() - pred) - epsilon;
		return err>0 ? err : 0;	
	}

	@Override
	public float getUpdateDirection(WeightVector w, SparseVector x) {
		float pred = clamp(w.innerProduct(x));
		return Math.abs(x.getY() - pred)>epsilon ? Math.signum(x.getY() - pred) : 0;			
	}

}
