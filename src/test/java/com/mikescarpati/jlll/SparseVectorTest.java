/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import static org.fest.assertions.Assertions.assertThat;

import org.testng.annotations.Test;


public class SparseVectorTest {
	
	@Test
	public void testVectorStringWithComment(){
		String testStr = "1.0 qid:2 1:1.0 2:2.5 4:-2 #happy";
		SparseVector x = new SparseVector(testStr);
		assertThat(x.getComment()).isEqualTo("happy");
		assertThat(x.getGroupId()).isEqualTo("2");
		assertThat(x.getY()).isEqualTo(1.0f);
		assertThat(x.valueAt(1)).isEqualTo(1.0f);
		assertThat(x.featureAt(1)).isEqualTo(1);
		assertThat(x.valueAt(2)).isEqualTo(2.5f);
		assertThat(x.getSquaredNorm()).isEqualTo(11.25f);
	}
	
	@Test
	public void testVectorStringNoComment(){
		String testStr = "1.0 1:1.0 2:2.5 4:-2\n";
		SparseVector x = new SparseVector(testStr);
		assertThat(x.getGroupId()).isEqualTo("0");
		assertThat(x.getY()).isEqualTo(1.0f);
		assertThat(x.valueAt(1)).isEqualTo(1.0f);
		assertThat(x.featureAt(1)).isEqualTo(1);
		assertThat(x.valueAt(2)).isEqualTo(2.5f);
		assertThat(x.getSquaredNorm()).isEqualTo(11.25f);
	}
	
	@Test
	public void testVectorStringTrailingSpace(){
		String testStr = "1.0 1:1.0      2:2.5   4:-2 \n";
		SparseVector x = new SparseVector(testStr);
		assertThat(x.getGroupId()).isEqualTo("0");
		assertThat(x.getY()).isEqualTo(1.0f);
		assertThat(x.valueAt(1)).isEqualTo(1.0f);
		assertThat(x.featureAt(1)).isEqualTo(1);
		assertThat(x.valueAt(2)).isEqualTo(2.5f);
		assertThat(x.getSquaredNorm()).isEqualTo(11.25f);
	}
	
	@Test
	public void testVectorDifferenceConstructor1(){
		String testStr1 = "1.0 qid:2 1:1.0 2:2.5 4:-2 #happy";
		String testStr2 = "1.0 1:1.0 2:2.5 4:-2\n";
		SparseVector x1 = new SparseVector(testStr1);
		SparseVector x2 = new SparseVector(testStr2);
		
		SparseVector x = new SparseVector(x1, x2, 0.0f);
		
		assertThat(x.getY()).isEqualTo(0.0f);
		assertThat(x.valueAt(1)).isEqualTo(0.0f);
	}

	@Test
	public void testVectorDifferenceConstructor2(){
		String testStr1 = "1.0 1:1.0 2:-1.0 3:4.5";
		String testStr2 = "0.0 2:2.0 4:3.0";
		
		SparseVector x = new SparseVector(new SparseVector(testStr1), 
				new SparseVector(testStr2), -1.0f);
		
		assertThat(x.getY()).isEqualTo(-1.0f);
		assertThat(x.valueAt(1)).isEqualTo(1.0f);
		assertThat(x.featureAt(1)).isEqualTo(1);
		assertThat(x.valueAt(2)).isEqualTo(-3.0f);
		assertThat(x.featureAt(2)).isEqualTo(2);
		assertThat(x.valueAt(3)).isEqualTo(4.5f);
		assertThat(x.featureAt(3)).isEqualTo(3);
		assertThat(x.valueAt(4)).isEqualTo(-3.0f);
		assertThat(x.featureAt(4)).isEqualTo(4);

	}


	@Test
	public void testVectorStringGroupId(){
		String testStr = "1.0 qid:3 1:1.0      2:2.5   4:-2 #note";
		SparseVector x = new SparseVector(testStr);
		assertThat(x.getGroupId()).isEqualTo("3");
		assertThat(x.getY()).isEqualTo(1.0f);
		assertThat(x.valueAt(1)).isEqualTo(1.0f);
		assertThat(x.featureAt(1)).isEqualTo(1);
		assertThat(x.valueAt(2)).isEqualTo(2.5f);
		assertThat(x.getSquaredNorm()).isEqualTo(11.25f);
		assertThat(x.getComment()).isEqualTo("note");
	}
	
	@Test
	public void testVectorStringComplexComment(){
		String testStr = "0 qid:15928 1:1.000000 2:0.000000 #docid = GX015-44-4118282 inc = 1 prob = 0.109181";
		SparseVector x = new SparseVector(testStr);
		assertThat(x.getGroupId()).isEqualTo("15928");
		assertThat(x.getY()).isEqualTo(0f);
		assertThat(x.valueAt(1)).isEqualTo(1.0f);
		assertThat(x.featureAt(1)).isEqualTo(1);
		assertThat(x.getComment()).isEqualTo("docid = GX015-44-4118282 inc = 1 prob = 0.109181");
	}
	

	@Test
	public void testVectorStringBias(){
		String testStr = "1.0 2:2.5 4:-2\n";
		SparseVector x = new SparseVector(testStr, true);
		assertThat(x.getGroupId()).isEqualTo("0");
		assertThat(x.getSquaredNorm()).isEqualTo(11.25f);
		assertThat(x.getY()).isEqualTo(1.0f);
		assertThat(x.valueAt(0)).isEqualTo(1.0f);
		assertThat(x.featureAt(0)).isEqualTo(0);
		assertThat(x.valueAt(2)).isEqualTo(-2.0f);
		assertThat(x.featureAt(2)).isEqualTo(4);
	}
	
	@Test
	public void testVectorStringNoBias(){
		String testStr = "1.0 2:2.5 4:-2\n";
		SparseVector x = new SparseVector(testStr, false);
		assertThat(x.getGroupId()).isEqualTo("0");
		assertThat(x.getSquaredNorm()).isEqualTo(10.25f);
		assertThat(x.getY()).isEqualTo(1.0f);
		assertThat(x.valueAt(0)).isEqualTo(0.0f);
		assertThat(x.featureAt(0)).isEqualTo(0);
		assertThat(x.valueAt(2)).isEqualTo(-2.0f);
		assertThat(x.featureAt(2)).isEqualTo(4);
	}

}
