/**
 * 
 */
package com.mikescarpati.jlll;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Used to hold the data for a single observation.  While most sparse vector
 * libraries use hash-map-backed storage, this uses an ArrayList for storage.
 * We do that because we never get the items randomly, we iterate over all items.
 * The is one of the performance-sensitive pieces of the code--in the pairwise 
 * approaches, over half of the time will be spent in initFromDifference.
 * @author Mike Scarpati
 *
 */
public class SparseVector implements Iterable<SingleFeature>{
	// Obtain a logger
	private static Logger logger = Logger.getLogger("SparseVector");
	// Typically, only non-zero valued features are stored.  This vector is assumed
	// to hold feature id, feature value pairs in order sorted by feature id.  The
	// special feature id 0 is always set to 1, encoding bias.
	private ArrayList<SingleFeature> features;

	// y_ is the class label.  We store this as a float, rather than an int,
	// so that this class may be used for regression problems, etc., if desired.
	private float y;

	// a_ is the current alpha value in optimization.
	private float a;

	// squared_norm_ = x1*x1 + ... + xN*xN
	private float squaredNorm;

	// Use this member when examples belong to distinct groups.  For instance,
	// in ranking problems examples are grouped by query id.  By default,
	// this is set to 0.
	private String groupId;

	// comment_ can be any string-based comment.
	private String comment;


	/**
	 * Construct a new vector from a string.  Input format is svm-light format
	 * <label> <feature>:<value> ... <feature:value> # comment<\n>
	 * No bias term is used.
	 * @param inString
	 */
	public SparseVector(String inString){
		y = 0.0f;
		a = 0.0f;
		features = new ArrayList<SingleFeature>();
		squaredNorm = 0.0f;
		groupId = "0";
		comment = "";
		noBias();
		assignFromSVMLight(inString);			  
	}

	/**
	 * This is inefficient to store dense values in such a sparse format, but
	 * it makes implementation easier.
	 */
	public SparseVector(float[] values, float y, boolean useBias){
		squaredNorm = 0.0f;
		a = 0.0f;
		features = new ArrayList<SingleFeature>();
		comment = "";
		if(useBias)
			setBias();
		else
			noBias();
		this.setY(y);
		for(int i=0; i<values.length; i++){
			this.pushPair(i+1, values[i]);
		}
	}
	/**
	 *  Constructs a new vector from a string, as above, but also sets the bias
	 *  term to 1 iff use_bias_term is set to true.
	 * @param inString
	 * @param useBiasTerm
	 */
	public SparseVector(String inString, boolean useBiasTerm){
		y = 0.0f;
		a = 0.0f;
		features = new ArrayList<SingleFeature>();
		squaredNorm = 0.0f;
		groupId = "0";
		comment = "";
		if(useBiasTerm)
			setBias();
		else
			noBias();
		assignFromSVMLight(inString);		  
	}

	/**
	 * Creates a new SparseVector from parallel arrays
	 * @param integers array where each element is the feature index
	 * @param floats array where each element is the value for each feature
	 * @param yVal the SparseVector's y-value
	 * @param useBiasTerm
	 */
	public SparseVector(Integer[] integers, Float[] floats, float yVal, boolean useBiasTerm){
		y = yVal;
		a = 0.0f;
		features = new ArrayList<SingleFeature>(integers.length);
		squaredNorm = 0.0f;
		groupId = "0";
		comment = "";
		if(useBiasTerm)
			setBias();
		else
			noBias();
		for(int i=0; i<integers.length; i++){
			pushPair(integers[i], floats[i]);
		}
	}

	/**
	 * creates a new SparseVector as scale * w * x, with y set to 0.
	 * @param w
	 * @param x
	 * @param scale
	 */
	public SparseVector(WeightVector w, SparseVector x, float scale){
		y = 0.0f;
		a = 0.0f;
		features = new ArrayList<SingleFeature>();
		squaredNorm = 0.0f;
		groupId = "0";
		comment = "";
		for(SingleFeature feat : x.features){
			pushPair(feat.index, scale * feat.value * w.getValue(feat.index));
		}
	}

	/**
	 *  Construct a new vector that is the difference of two vectors, (u - v).
	 *  This is useful for ranking problems, etc.
	 * @param u
	 * @param v
	 * @param inputY
	 */
	public SparseVector(SparseVector u, SparseVector v, float inputY){
		initFromDifference(u, v, inputY);
	}
	/**
	 * Construct a new vector that is the difference of two vectors, (u - v).
	 * Similar to SparseVector(SparseVector u, SparseVector v, float inputY), but
	 * calculates Y automatically. If binarize is true, Y will be +/- 1, otherwise
	 * it will be the difference between the y's. 
	 * @param u
	 * @param v
	 * @param binarize
	 */
	public SparseVector(SparseVector u, SparseVector v, boolean binarize){
		float newY;
		if(binarize){
			newY = ((u.getY() > v.getY()) ? 1.0f :
				(u.getY() < v.getY()) ? -1.0f : 0.0f);
		} else{
			newY = u.getY() - v.getY();
		}
		initFromDifference(u, v, newY);
	}




	/**
	 * empty constructor for use in shallowClone.
	 */
	private SparseVector(){
		y = 0.0f;
		a = 0.0f;
		features = new ArrayList<SingleFeature>();
		squaredNorm = 0.0f;
		groupId = "";
	}

	/**
	 * Makes a shallow clone, primarily for cluster mapping.
	 * Note that this doesn't put anything in the bias slot.
	 */
	public SparseVector shallowClone(){
		SparseVector x = new SparseVector();
		x.y = this.y;
		x.a = this.a;
		x.groupId = this.groupId;
		x.comment = this.comment;
		x.squaredNorm = 0;

		return x;
	}

	@Override
	public SparseVector clone(){
		SparseVector x = shallowClone();
		for(SingleFeature feature : features){
			x.features.add(feature);
			x.squaredNorm += feature.value * feature.value;
		}
		return x;
	}

	public SparseVector getSquared(){
		SparseVector x = shallowClone();
		for(SingleFeature feature : features){
			float val = feature.value * feature.value;
			x.features.add(new SingleFeature(feature.index, val));
			x.squaredNorm += val * val;
		}
		return x;

	}

	/**
	 * Initializes this SparseVector as the difference of u and v. 
	 * @param u 
	 * @param v
	 * @param inputY What should the y-value of the resulting SparseVector be?
	 */
	private void initFromDifference(SparseVector u, SparseVector v, float inputY){
		y = inputY;
		a = 0.0f;
		//arbitrarily pick first for size to speed things up a little
		features = new ArrayList<SingleFeature>(u.numFeatures()); 
		squaredNorm = 0.0f;
		groupId = u.groupId;
		int ui = 0;
		int vi = 0;
		int uLen = u.numFeatures();
		int vLen = v.numFeatures();
		while (ui < uLen || vi < vLen) {
			// u has no features remaining.
			if (!(ui < uLen)) {
				while(vi < vLen){
					pushPair(v.featureAt(vi), 0.0 - v.valueAt(vi));
					++vi;
				}
				return;
			}
			// v has no features remaining.
			if (!(vi < vLen)) {
				while(ui < uLen){
					pushPair(u.featureAt(ui), u.valueAt(ui));
					++ui;					
				}
				return;
			}
			// u's current feature is less than v's current feature.
			if (v.featureAt(vi) < u.featureAt(ui)) {
				pushPair(v.featureAt(vi), 0.0 - v.valueAt(vi));
				++vi;
				continue;
			}
			// v's current feature is less than u's current feature.
			if (u.featureAt(ui) < v.featureAt(vi)) {
				pushPair(u.featureAt(ui), u.valueAt(ui));
				++ui;
				continue;
			}

			// a_i and b_i are pointing to the same feature.
			pushPair(u.featureAt(ui), u.valueAt(ui) - v.valueAt(vi));
			++ui;
			++vi;
		}
	}

	/**
	 * Returns <this, other>
	 * @param other
	 */
	public float innerProduct(SparseVector other){

		int ui = 0;
		int vi = 0;
		int uLen = this.numFeatures();
		int vLen = other.numFeatures();
		float sum = 0f;
		while (ui < uLen && vi < vLen) { // either one means no more contribution

			// u's current feature is less than v's current feature.
			if (this.featureAt(vi) < other.featureAt(ui)) {
				++vi;
				continue;
			}
			// v's current feature is less than u's current feature.
			if (this.featureAt(ui) < other.featureAt(vi)) {
				++ui;
				continue;
			}
			// pointing to the same feature.
			sum += this.valueAt(ui) * other.valueAt(vi);
			++ui;
			++vi;
		}
		return sum;
	}
	protected void addToSquaredNorm(float addend) { squaredNorm += addend; }

	/**
	 *  Common initialization method shared by constructors, adding vector data
	 *  by parsing a string in SVM-light format.
	 * @param inString
	 */
	protected void assignFromSVMLight(String inString){
		int length = inString.length();
		if (length == 0) dieFormat("Empty example string.");
		StringTokenizer st = new StringTokenizer(inString);
		String token = null;
		String[] parts;

		// Get class label.
		try{
			y = Float.parseFloat(st.nextToken());
		} catch(Exception e) {
			dieFormat("Class label must be real number.");
		}

		// Parse the group id, if any, or the first feature:value pair
		try{
			token = st.nextToken();

			parts = token.split(":");
			if(parts[0].equals("qid")){
				groupId = parts[1];
			} else{
				try{
					pushPair(Integer.parseInt(parts[0]), Float.parseFloat(parts[1]));
				} catch(NumberFormatException e){
					String[] sep = parts[1].split("#");
					pushPair(Integer.parseInt(parts[0]), Float.parseFloat(sep[0]));
					comment = sep[1];
				}
			}

		} catch(Exception e) {
			dieFormat("Invalid token at qid: " + token);
		}

		// Get feature:value pairs and optional comment
		while(st.hasMoreTokens()){
			token = st.nextToken();
			try{
				parts = token.split(":",2);
				if(parts.length==2){ //should only happen for feat:val pairs
					try{
						pushPair(Integer.parseInt(parts[0]), Float.parseFloat(parts[1]));
					} catch(NumberFormatException e){
						String[] sep = parts[1].split("#");
						pushPair(Integer.parseInt(parts[0]), Float.parseFloat(sep[0]));
						comment = sep[1];
					}
				}else if(parts[0].startsWith("#")){
					comment = parts[0].substring(1);
					if(st.hasMoreTokens()){ // get everything else with no delim
						comment += st.nextToken("");
					}
					//comment = parts[0].substring(1);
				}else{
					dieFormat("Invalid token after qid: " + token);
				}
			} catch(Exception e){
				dieFormat("Invalid token: " + token);
			}
		}
	}

	/**
	 * Sets up the bias term, indexed by feature id 0.
	 */
	void setBias(){ 
		pushPair(0, 1); 
	}

	/**
	 * Sets up the bias term as null value, indexed by feature id 0.
	 */
	void noBias() { 
		pushPair(0, 0); 
	}

	/**
	 * Returns a string-format representation of the vector, in svm-light format.
	 */
	@Override
	public String toString(){
		String out = Float.toString(y) + " ";
		for(int i=0; i<numFeatures(); ++i){
			out += Integer.toString(features.get(i).index) + ":" + 
					Double.toString(features.get(i).value) + " ";
		}
		if(comment != null){
			if(comment.length()>0){
				out += "#" + comment;
			}
		}
		return out;
	}

	// Methods for interacting with features
	int numFeatures() { return features.size(); }
	int featureAt(int i) { return features.get(i).index; }
	double valueAt(int i) { return features.get(i).value; }

	// Getters and setters.
	public void setY(float new_y) { y = new_y; }
	public void setA(float new_a) { a = new_a; }
	public void setGroupId(String new_id) { groupId = new_id; }
	public void setComment(String new_comment) { comment = new_comment; }
	public float getY()  { return y; }
	public float getA()  { return a; }
	public float getSquaredNorm()  { return squaredNorm; }
	public String getGroupId()  { return groupId; }
	public String getComment()  { return comment; }

	/**
	 * Adds a new (id, value) FeatureValuePair to the end of the vector, and
	 * updates the internal squared_norm_ member.
	 * @param id
	 * @param value
	 */
	void pushPair (int id, float value){
		features.add(new SingleFeature(id, value));
		squaredNorm += value*value;
	}

	/**
	 * Adds a new (id, value) FeatureValuePair to the end of the vector, and
	 * updates the internal squared_norm_ member.
	 * @param id
	 * @param value
	 */
	void pushPair (int id, double value){
		features.add(new SingleFeature(id, value));
		squaredNorm += value*value;
	}

	/**
	 * Not sure if this is needed, but keeping to make this similar to the 
	 * original implementation.
	 * @param reason
	 * @throws IllegalArgumentException
	 */
	void dieFormat(String reason) throws IllegalArgumentException{
		logger.severe("Wrong format for input data: " + reason);
		throw new IllegalArgumentException();
	}

	public Iterator<SingleFeature> iterator() {
		Iterator<SingleFeature> it = new Iterator<SingleFeature>() {

			private int currentIndex = 0;

			@Override
			public boolean hasNext() {
				return currentIndex < features.size();
			}

			@Override
			public SingleFeature next() {
				return features.get(currentIndex++);
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
			}
		};
		return it;
	}
	
	/**
	 * Gives a new SparseVector where each element is xi/wi^2
	 * @param x
	 * @return
	 */
	public SparseVector elementwiseInverseSquare(WeightVector w){
		SparseVector ret = new SparseVector();
		for(SingleFeature feat : features){
			float tmp = w.getValue(feat.index);
			if(tmp>0){
				ret.pushPair(feat.index, feat.value / (tmp * tmp));				
			} else{
				ret.pushPair(feat.index, 0);
			}
			
		}
		return ret;
	}
	/**
	 * Gives a new SparseVector where each element is xi/wi
	 * @param x
	 * @return
	 */
	public SparseVector elementwiseInverse(WeightVector w){
		SparseVector ret = new SparseVector();
		for(SingleFeature feat : features){
			float tmp = w.getValue(feat.index);
			if(tmp>0){
				ret.pushPair(feat.index, feat.value / tmp);				
			} else{
				ret.pushPair(feat.index, 0);
			}
			
		}
		return ret;
	}
	/**
	 * Gives a new SparseVector where each element is xi/sqrt(wi)
	 * @param x
	 * @return
	 */
	public SparseVector elementwiseInverseSqrt(WeightVector w){
		SparseVector ret = new SparseVector();
		for(SingleFeature feat : features){
			float tmp = w.getValue(feat.index);
			if(tmp>0){
				ret.pushPair(feat.index, feat.value / Math.sqrt(tmp));				
			} else{
				ret.pushPair(feat.index, 0);
			}
			
		}
		return ret;
	}
}
