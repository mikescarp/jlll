/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.losses;

import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;

/**
 * Exponential loss for boosting-like classification or ranking.
 * @author Mike Scarpati
 *
 */
public class LogRegLoss extends LossType {
	
	@Override
	public float predict(WeightVector w, SparseVector x) {
		float p = w.innerProduct(x);
		return (float) (1 / (1.0 + Math.exp(-p)));
	}

	/**
	 * Not the true loss, but a faster proxy: exp(-y*w*x)
	 */
	@Override
	public float getLoss(WeightVector w, SparseVector x) {
		return (float) Math.exp(-x.getY() * w.innerProduct(x));
	}

	@Override
	public float getUpdateDirection(WeightVector w, SparseVector x) {
		return (float) (x.getY() / (1 + Math.exp(x.getY() * w.innerProduct(x))));
	}
}
