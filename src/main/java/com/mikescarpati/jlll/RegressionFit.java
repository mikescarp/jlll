/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll;

import java.util.ArrayList;

/**
 * Returns a couple of simple fit metrics for regression problems
 * @author Mike Scarpati
 *
 */
public class RegressionFit {
	ArrayList<Float> trueLabels;
	ArrayList<Float> predictedLabels;
	
	public RegressionFit(ArrayList<Float> trueLabels, ArrayList<Float> predictedLabels){
		this.trueLabels = trueLabels;
		this.predictedLabels = predictedLabels;
	}
	
	public float getRMSE(){
		float sse = 0;
		for(int i=0; i<trueLabels.size(); i++){
			sse += Math.pow((trueLabels.get(i) - predictedLabels.get(i)), 2);
		}
		return (float) Math.sqrt(sse/trueLabels.size());
	}
	
	public float getMAE(){
		float sae = 0;
		for(int i=0; i<trueLabels.size(); i++){
			sae += Math.abs(trueLabels.get(i) - predictedLabels.get(i));
		}
		return sae/trueLabels.size();
	}
}
