/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.WeightUpdates;

import com.mikescarpati.jlll.OptimizationContext;
import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.losses.LossType;

/**
 * Implements the Primal Estimated sub-GrAdient SOlver for SVM.
 * See http://eprints.pascal-network.org/archive/00004062/01/ShalevSiSr07.pdf
 * for details. Note that we don't just solve for SVM, this can work
 * with other types of loss functions by following the basic procedure of
 * regularize, step, project
 * @author moxienew
 *
 */
public class PegasosUpdater implements WeightUpdater {

	public WeightVector takeStep(WeightVector w, SparseVector x,
			OptimizationContext oc, LossType p) {
		float loss = p.getLoss(w, x);
		try {
			w.l2Regularize(oc.getRate(), oc.getLambda());
			if(loss > 0){
				w.addVector(x, oc.getRate() * p.getUpdateDirection(w, x));
			}
			w.pegasosProjection(oc.getLambda());
			return w;
		} catch (Exception e) {
			// Numerical problems
			e.printStackTrace();
		}
		return w;
	}

	public WeightUpdater shallowClone() {
		return new PegasosUpdater();
	}

}
