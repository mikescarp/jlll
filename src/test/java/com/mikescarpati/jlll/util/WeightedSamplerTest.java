package com.mikescarpati.jlll.util;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.testng.annotations.Test;
import static org.fest.assertions.Assertions.assertThat;

public class WeightedSamplerTest {

	@Test
	public void testSampling(){
		List<Float> weights = new LinkedList<Float>();
		weights.add(0.1f);
		weights.add(0.3f);
		weights.add(0.05f);
		weights.add(0.15f);
		weights.add(0.05f);
		weights.add(0.2f);
		weights.add(0.15f);
		
		WeightedSampler sampler = new WeightedSampler(weights);
		int[] results = new int[weights.size()];
		Arrays.fill(results, 0);
		for(int i=0; i<5000; i++){
			int idx = sampler.nextIndex();
			results[idx] = results[idx] + 1;
		}
		assertThat(results[1]).isGreaterThan(results[0]);
		assertThat(results[0]).isGreaterThan(results[4]);
		assertThat(results[6]).isGreaterThan(results[2]);
		System.out.println(Arrays.toString(results));
	}
}
