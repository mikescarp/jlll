/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.rates;

/**
 * Rate used in PEGASOS: 1/(lambda*i)
 * @author Mike Scarpati
 *
 */
public class PegasosRate implements LearningRate {

	int i;
	float lambda;
	
	public PegasosRate(float lambda){
		i = 1;
		this.lambda = lambda;
	}
	
	public float getRate() {
		return (1.0f / (lambda * i));
	}

	@Override
	public void increment() {
		i++;		
	}

	@Override
	public void reset() {
		i=1;
		
	}

	@Override
	public int getIteration() {
		return i;
	}

}
