/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.WeightUpdates;

import com.mikescarpati.jlll.OptimizationContext;
import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.losses.LossType;

/**
 * SGD with regularization.  See T. Zhang. Solving large scale linear prediction 
 * problems using stochastic gradient descent algorithms. In ICML �04: Proceedings 
 * of the twenty-first international conference on Machine learning, 2004 for details.
 * @author Mike Scarpati
 *
 */
public class SgdUpdater implements WeightUpdater {

	public WeightVector takeStep(WeightVector w, SparseVector x,
								 OptimizationContext oc, LossType p) {
		float loss = p.getLoss(w, x);
		try {
			w.l2Regularize(oc.getRate(), oc.getLambda());
			
			if(loss > 0){
				w.addVector(x, oc.getRate() * p.getUpdateDirection(w, x));
			}
			return w;
		} catch (Exception e) {
			// Numerical problems
			e.printStackTrace();
		}
		return w;
	}

	public WeightUpdater shallowClone() {
		return new SgdUpdater();
	}

}
