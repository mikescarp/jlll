/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.WeightUpdates;

import com.mikescarpati.jlll.OptimizationContext;
import com.mikescarpati.jlll.SparseVector;
import com.mikescarpati.jlll.WeightVector;
import com.mikescarpati.jlll.losses.LossType;
/**
 * Takes a single Passive-Aggressive step, including projection if
 * lambda is greater than 0.0.
 * This corresponds to the PA-II algorithm in
 * http://jmlr.org/papers/volume7/crammer06a/crammer06a.pdf 
 * 
 * @author Mike Scarpati
 *
 */
public class PassiveAggressiveTwoUpdater implements WeightUpdater {

	private final float capacity;
	
	/**
	 * Creates a new PA-II updater with capacity parameter c
	 * @param capacity as defined in the paper; small values encourage simpler models
	 */
	public PassiveAggressiveTwoUpdater(float capacity){
		this.capacity = 1 / (2*capacity); // don't need to re-divide each time
	}
	/**
	 * Uses a default of 10 for the capacity. Small values encourage simpler models.
	 */
	public PassiveAggressiveTwoUpdater(){
		this.capacity = 1 / (2 * 10.0f); // don't need to re-divide each time
	}
	
	public WeightVector takeStep(WeightVector w, SparseVector x,
								 OptimizationContext oc, LossType p) {
		float loss = p.getLoss(w, x);
		if(loss > 0){
			try {
				float stepSize = loss / (x.getSquaredNorm() + capacity);
				w.addVector(x, stepSize * p.getUpdateDirection(w, x));
				if(oc.getLambda()>0f){
					w.pegasosProjection(oc.getLambda());
				}
			} catch (Exception e) {
				// Numerical Problems
				e.printStackTrace();
			}
		}
		return w;
	}

	public WeightUpdater shallowClone() {
		// TODO Auto-generated method stub
		return new PassiveAggressiveTwoUpdater(1/(2*capacity));
	}

}
