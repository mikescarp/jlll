/*******************************************************************************
 * Copyright 2013 Mike Scarpati
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.mikescarpati.jlll.rates;

import org.fest.assertions.Delta;
import org.testng.annotations.DataProvider;

public class RateTestCases {

	static Delta delta = Delta.delta(0.000001f);
	
	@DataProvider(name = "rates")
	public static Object[][] createRateTestData() {
	 return new Object[][] {
	   { new Integer(5) },
	   { new Integer(50)},
	   { new Integer(250)},
	   { new Integer(12345)},
	 };
	}
	
	@DataProvider(name = "ratesWithLambda")
	public static Object[][] createRateLambdaTestData() {
	 return new Object[][] {
	   { new Integer(5) , new Float(0.1)},
	   { new Integer(50) , new Float(1)},
	   { new Integer(250) , new Float(3)},
	   { new Integer(12345) , new Float(0.001)},
	 };
	}
	
	@DataProvider(name = "lambdas")
	public static Object[][] createLambdaTestData() {
	 return new Object[][] {
	   { new Float(0.1)},
	   { new Float(1)},
	   { new Float(3)},
	   { new Float(0.001)},
	 };
	}
}
